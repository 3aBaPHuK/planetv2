<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\RecieptCategoryRepository")
 */
class RecieptCategory
{
    public function __toString()
    {
        return $this->getName();
    }

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Reciept", mappedBy="recieptCategory", orphanRemoval=true)
     */
    private $reciepts;

    public function __construct()
    {
        $this->reciepts = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|Reciept[]
     */
    public function getReciepts(): Collection
    {
        return $this->reciepts;
    }

    public function addReciept(Reciept $reciept): self
    {
        if (!$this->reciepts->contains($reciept)) {
            $this->reciepts[] = $reciept;
            $reciept->setRecieptCategory($this);
        }

        return $this;
    }

    public function removeReciept(Reciept $reciept): self
    {
        if ($this->reciepts->contains($reciept)) {
            $this->reciepts->removeElement($reciept);
            // set the owning side to null (unless already changed)
            if ($reciept->getRecieptCategory() === $this) {
                $reciept->setRecieptCategory(null);
            }
        }

        return $this;
    }
}
