<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProductPropertiesRepository")
 */
class ProductProperties
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $article;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $bonus;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $instructions;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $parameters;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $examples;

    /**
     * @ORM\Column(type="string", length=30, nullable=true)
     */
    private $size;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $rating;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Product", inversedBy="productProperties", cascade={"persist", "remove"})
     */
    private $product;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Reciept")
     */
    private $reciepts;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Image", mappedBy="productProperties", cascade={"persist", "remove"})
     */
    private $additionalImages;

    public function __construct()
    {
        $this->reciepts = new ArrayCollection();
        $this->additionalImages = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getArticle(): ?string
    {
        return $this->article;
    }

    public function setArticle(?string $article): self
    {
        $this->article = $article;

        return $this;
    }

    public function getBonus(): ?int
    {
        return $this->bonus;
    }

    public function setBonus(?int $bonus): self
    {
        $this->bonus = $bonus;

        return $this;
    }

    public function getInstructions(): ?string
    {
        return $this->instructions;
    }

    public function setInstructions(?string $instructions): self
    {
        $this->instructions = $instructions;

        return $this;
    }

    public function getParameters(): ?string
    {
        return $this->parameters;
    }

    public function setParameters(?string $parameters): self
    {
        $this->parameters = $parameters;

        return $this;
    }

    public function getExamples(): ?string
    {
        return $this->examples;
    }

    public function setExamples(?string $examples): self
    {
        $this->examples = $examples;

        return $this;
    }

    public function getSize(): ?string
    {
        return $this->size;
    }

    public function setSize(string $size): self
    {
        $this->size = $size;

        return $this;
    }

    public function getRating(): ?int
    {
        return $this->rating;
    }

    public function setRating(?int $rating): self
    {
        $this->rating = $rating;

        return $this;
    }

    public function getProduct(): ?Product
    {
        return $this->product;
    }

    public function setProduct(?Product $product): self
    {
        $this->product = $product;

        return $this;
    }

    /**
     * @return Collection|Reciept[]
     */
    public function getReciepts(): Collection
    {
        return $this->reciepts;
    }

    public function addReciept(Reciept $reciept): self
    {
        if (!$this->reciepts->contains($reciept)) {
            $this->reciepts[] = $reciept;
        }

        return $this;
    }

    public function removeReciept(Reciept $reciept): self
    {
        if ($this->reciepts->contains($reciept)) {
            $this->reciepts->removeElement($reciept);
        }

        return $this;
    }

    /**
     * @return Collection|Image[]
     */
    public function getAdditionalImages(): Collection
    {
        return $this->additionalImages;
    }

    public function addAdditionalImage(Image $additionalImage): self
    {
        if (!$this->additionalImages->contains($additionalImage)) {
            $this->additionalImages[] = $additionalImage;
            $additionalImage->setProductProperties($this);
        }

        return $this;
    }

    public function removeAdditionalImage(Image $additionalImage): self
    {
        if ($this->additionalImages->contains($additionalImage)) {
            $this->additionalImages->removeElement($additionalImage);
            // set the owning side to null (unless already changed)
            if ($additionalImage->getProductProperties() === $this) {
                $additionalImage->setProductProperties(null);
            }
        }

        return $this;
    }
}
