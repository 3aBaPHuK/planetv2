<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\OfferRepository")
 */
class Offer implements \JsonSerializable
{
    public function __toString()
    {
        return $this->getName();
    }

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $code;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $externalCode;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $archived;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $pathName;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $minPrice;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $stock;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $quantity;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $article;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\SalePrice", mappedBy="offer", cascade={"persist"}, orphanRemoval=true)
     */
    private $SalePrices;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $weight;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $volume;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $fake;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $fromEntity;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $fromEntityId;

    public function __construct()
    {
        $this->SalePrices = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(?string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getExternalCode(): ?string
    {
        return $this->externalCode;
    }

    public function setExternalCode(?string $externalCode): self
    {
        $this->externalCode = $externalCode;

        return $this;
    }

    public function getArchived(): ?bool
    {
        return $this->archived;
    }

    public function setArchived(?bool $archived): self
    {
        $this->archived = $archived;

        return $this;
    }

    public function getPathName(): ?string
    {
        return $this->pathName;
    }

    public function setPathName(string $pathName): self
    {
        $this->pathName = $pathName;

        return $this;
    }

    public function getMinPrice(): ?float
    {
        return $this->minPrice;
    }

    public function setMinPrice(?float $minPrice): self
    {
        $this->minPrice = $minPrice;

        return $this;
    }

    public function getStock(): ?int
    {
        return $this->stock;
    }

    public function setStock(?int $stock): self
    {
        $this->stock = $stock;

        return $this;
    }

    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    public function setQuantity(?int $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }

    public function getArticle(): ?string
    {
        return $this->article;
    }

    public function setArticle(?string $article): self
    {
        $this->article = $article;

        return $this;
    }

    /**
     * @return Collection|SalePrice[]
     */
    public function getSalePrices(): Collection
    {
        return $this->SalePrices;
    }

    public function addSalePrice(SalePrice $salePrice): self
    {
        if (!$this->SalePrices->contains($salePrice)) {
            $this->SalePrices[] = $salePrice;
            $salePrice->setOffer($this);
        }

        return $this;
    }

    public function removeSalePrice(SalePrice $salePrice): self
    {
        if ($this->SalePrices->contains($salePrice)) {
            $this->SalePrices->removeElement($salePrice);
            // set the owning side to null (unless already changed)
            if ($salePrice->getOffer() === $this) {
                $salePrice->setOffer(null);
            }
        }

        return $this;
    }

    public function getWeight(): ?float
    {
        return $this->weight;
    }

    public function setWeight(?float $weight): self
    {
        $this->weight = $weight;

        return $this;
    }

    public function getVolume(): ?float
    {
        return $this->volume;
    }

    public function setVolume(?float $volume): self
    {
        $this->volume = $volume;

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function jsonSerialize()
    {
        return get_object_vars($this);
    }

    public function getFake(): ?bool
    {
        return $this->fake;
    }

    public function setFake(?bool $fake): self
    {
        $this->fake = $fake;

        return $this;
    }

    public function getFromEntity(): ?string
    {
        return $this->fromEntity;
    }

    public function setFromEntity(?string $fromEntity): self
    {
        $this->fromEntity = $fromEntity;

        return $this;
    }

    public function getFromEntityId(): ?int
    {
        return $this->fromEntityId;
    }

    public function setFromEntityId(?int $fromEntityId): self
    {
        $this->fromEntityId = $fromEntityId;

        return $this;
    }
}
