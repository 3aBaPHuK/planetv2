<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ShopRepository")
 */
class Shop
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="text")
     */
    private $address;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $supplier;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $shipper;

    /**
     * @ORM\Column(type="text")
     */
    private $bankDetailsName;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $bankDetailsINN;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $bankDetailsKPP;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $bankDetailsBIK;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $bankDetailsCheckingAccount;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $bankDetailsCorrespondedAccount;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $bankDetailsAccountHolderName;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $addressDescription;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $phone;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getSupplier(): ?string
    {
        return $this->supplier;
    }

    public function setSupplier(string $supplier): self
    {
        $this->supplier = $supplier;

        return $this;
    }

    public function getShipper(): ?string
    {
        return $this->shipper;
    }

    public function setShipper(string $shipper): self
    {
        $this->shipper = $shipper;

        return $this;
    }

    public function getBankDetailsName(): ?string
    {
        return $this->bankDetailsName;
    }

    public function setBankDetailsName(string $bankDetailsName): self
    {
        $this->bankDetailsName = $bankDetailsName;

        return $this;
    }

    public function getBankDetailsINN(): ?string
    {
        return $this->bankDetailsINN;
    }

    public function setBankDetailsINN(string $bankDetailsINN): self
    {
        $this->bankDetailsINN = $bankDetailsINN;

        return $this;
    }

    public function getBankDetailsKPP(): ?string
    {
        return $this->bankDetailsKPP;
    }

    public function setBankDetailsKPP(string $bankDetailsKPP): self
    {
        $this->bankDetailsKPP = $bankDetailsKPP;

        return $this;
    }

    public function getBankDetailsBIK(): ?string
    {
        return $this->bankDetailsBIK;
    }

    public function setBankDetailsBIK(string $bankDetailsBIK): self
    {
        $this->bankDetailsBIK = $bankDetailsBIK;

        return $this;
    }

    public function getBankDetailsCheckingAccount(): ?string
    {
        return $this->bankDetailsCheckingAccount;
    }

    public function setBankDetailsCheckingAccount(string $bankDetailsCheckingAccount): self
    {
        $this->bankDetailsCheckingAccount = $bankDetailsCheckingAccount;

        return $this;
    }

    public function getBankDetailsCorrespondedAccount(): ?string
    {
        return $this->bankDetailsCorrespondedAccount;
    }

    public function setBankDetailsCorrespondedAccount(string $bankDetailsCorrespondedAccount): self
    {
        $this->bankDetailsCorrespondedAccount = $bankDetailsCorrespondedAccount;

        return $this;
    }

    public function getBankDetailsAccountHolderName(): ?string
    {
        return $this->bankDetailsAccountHolderName;
    }

    public function setBankDetailsAccountHolderName(string $bankDetailsAccountHolderName): self
    {
        $this->bankDetailsAccountHolderName = $bankDetailsAccountHolderName;

        return $this;
    }

    public function getAddressDescription(): ?string
    {
        return $this->addressDescription;
    }

    public function setAddressDescription(?string $addressDescription): self
    {
        $this->addressDescription = $addressDescription;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }
}
