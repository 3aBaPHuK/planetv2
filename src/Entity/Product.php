<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProductRepository")
 * @Vich\Uploadable
 */
class Product implements \JsonSerializable
{

    public const IMPORTED_PROPERTIES = [
        'name',
        'quantity',
        'price',
        'image_main',
        'image',
        'announce',
        'description',
        'measurement',
        'currency',
        'width',
        'height',
        'length',
        'additionalImages',
        'article',
        'bonus',
        'instructions',
        'parameters',
        'examples',
        'size',
        'rating',
        'product',
        'reciepts',
        'category1',
        'category2',
        'category3'
    ];

    public function __toString()
    {
        return $this->getName() ?? '';
    }

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="integer")
     */
    private $quantity;

    /**
     * @ORM\Column(type="float")
     */
    private $price;

    /**
     * @ORM\Column(type="text")
     */
    private $announce;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Currency")
     * @ORM\JoinColumn(nullable=false)
     */
    private $currency;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Measurement")
     */
    private $measurement;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $width;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $height;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $length;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Category", inversedBy="products")
     * @ORM\JoinColumn(nullable=true)
     */
    private $category;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ProductImage", mappedBy="product", cascade={"remove", "persist"}, orphanRemoval=true)
     */
    private $imageCollection;

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $offerCodes = [];

    /**
     * @Gedmo\Slug(fields={"name"})
     * @ORM\Column(type="string", length=128, unique=true, nullable=true)
     */
    private $slug;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\ProductProperties", mappedBy="product", cascade={"persist", "remove"})
     */
    private $productProperties;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ProductOffer", mappedBy="product", cascade={"persist", "remove"})
     */
    private $productOffers;

    public function __construct()
    {
        $this->imageCollection = new ArrayCollection();
        $this->productOffers = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    public function setQuantity(int $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getAnnounce(): ?string
    {
        return $this->announce;
    }

    public function setAnnounce(string $announce): self
    {
        $this->announce = $announce;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getCurrency(): ?Currency
    {
        return $this->currency;
    }

    public function setCurrency(?Currency $currency): self
    {
        $this->currency = $currency;

        return $this;
    }

    public function getMeasurement(): ?Measurement
    {
        return $this->measurement;
    }

    public function setMeasurement(?Measurement $measurement): self
    {
        $this->measurement = $measurement;

        return $this;
    }

    public function getWidth(): ?float
    {
        return $this->width;
    }

    public function setWidth(?float $width): self
    {
        $this->width = $width;

        return $this;
    }

    public function getHeight(): ?float
    {
        return $this->height;
    }

    public function setHeight(?float $height): self
    {
        $this->height = $height;

        return $this;
    }

    public function getLength(): ?float
    {
        return $this->length;
    }

    public function setLength(?float $lenght): self
    {
        $this->length = $lenght;

        return $this;
    }

    public function getCategory(): ?Category
    {
        return $this->category;
    }

    public function setCategory(?Category $category): self
    {
        $this->category = $category;

        return $this;
    }

    /**
     * @return Collection|ProductImage[]
     */
    public function getImageCollection(): Collection
    {
        return $this->imageCollection;
    }

    public function addImageCollection(?ProductImage $imageCollection): self
    {
        if (!$this->imageCollection->contains($imageCollection)) {
            if (!$imageCollection->getImage() && $imageCollection->getImageFile()) {
                $imageCollection->setImage($imageCollection->getImageFile()->getBasename());
            }

            $this->imageCollection[] = $imageCollection;
            $imageCollection->setProduct($this);
        }

        return $this;
    }

    public function removeImageCollection(ProductImage $imageCollection): self
    {
        if ($this->imageCollection->contains($imageCollection)) {
            $this->imageCollection->removeElement($imageCollection);
            // set the owning side to null (unless already changed)
            if ($imageCollection->getProduct() === $this) {
                $imageCollection->setProduct(null);
            }
        }

        return $this;
    }

    public function getOfferCodes(): ?array
    {
        return $this->offerCodes;
    }

    public function setOfferCodes(?array $offerCodes): self
    {
        $this->offerCodes = $offerCodes;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getMainImage() {
        
        foreach ($this->getImageCollection()->getValues() as $image) {
            if ($image->getMain()) {
                return $image;
            }
        }

        return null;
    }

    /**
     * @inheritDoc
     */
    public function jsonSerialize()
    {
        return get_object_vars($this);
    }

    public function getProductProperties(): ?ProductProperties
    {
        return $this->productProperties;
    }

    public function setProductProperties(?ProductProperties $productProperties): self
    {
        $this->productProperties = $productProperties;

        // set (or unset) the owning side of the relation if necessary
        $newProduct = null === $productProperties ? null : $this;
        if ($productProperties->getProduct() !== $newProduct) {
            $productProperties->setProduct($newProduct);
        }

        return $this;
    }

    /**
     * @return Collection|ProductOffer[]
     */
    public function getProductOffers(): Collection
    {
        return $this->productOffers;
    }

    public function addProductOffer(ProductOffer $productOffer): self
    {
        if (!$this->productOffers->contains($productOffer)) {
            $this->productOffers[] = $productOffer;
            $productOffer->setProduct($this);
        }

        return $this;
    }

    public function removeProductOffer(ProductOffer $productOffer): self
    {
        if ($this->productOffers->contains($productOffer)) {
            $this->productOffers->removeElement($productOffer);
            // set the owning side to null (unless already changed)
            if ($productOffer->getProduct() === $this) {
                $productOffer->setProduct(null);
            }
        }

        return $this;
    }
}
