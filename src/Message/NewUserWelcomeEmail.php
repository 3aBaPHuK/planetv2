<?php
/**
 * Created by Elnikov.A
 * User: elnikov.a
 * Date: 05.05.2020
 * Time: 17:41
 */

namespace App\Message;

use App\Entity\Shop;

class NewUserWelcomeEmail
{

    /**
     * @var string
     */
    private $userId;

    /**
     * @var string
     */
    private $userActivationLink;

    /**
     * @var string
     */
    private $baseUrl;

    /**
     * @var Shop
     */
    private $shop;

    public function __construct(string $userId, string $userActivationLink, string $baseUrl = '', Shop $shop = null)
    {
        $this->userId             = $userId;
        $this->userActivationLink = $userActivationLink;
        $this->baseUrl            = $baseUrl;
        $this->shop               = $shop;
    }

    /**
     * @return string
     */
    public function getUserId(): string
    {
        return $this->userId;
    }

    /**
     * @return string
     */
    public function getBaseUrl(): string
    {
        return $this->baseUrl;
    }

    public function getShop()
    {
        return $this->shop;
    }

    /**
     * @return string
     */
    public function getUserActivationLink(): string
    {
        return $this->userActivationLink;
    }
}