<?php
/**
 * Created by Elnikov.A
 * User: elnikov.a
 * Date: 05.05.2020
 * Time: 17:41
 */

namespace App\Message;

use App\Entity\Shop;

class OrderStatusChangedEmail
{

    /**
     * @var string
     */
    private $orderId;

    /**
     * @var string
     */
    private $baseUrl;

    /**
     * @var Shop
     */
    private $shop;

    public function __construct(string $orderId, string $baseUrl = '', Shop $shop = null)
    {
        $this->orderId = $orderId;
        $this->baseUrl = $baseUrl;
        $this->shop    = $shop;
    }

    /**
     * @return string
     */
    public function getOrderId(): string
    {
        return $this->orderId;
    }

    public function getBaseUrl()
    {
        return $this->baseUrl;
    }

    public function getShop()
    {
        return $this->shop;
    }
}