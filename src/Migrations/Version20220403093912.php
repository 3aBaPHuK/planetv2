<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20220403093912 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Added position for categories order';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE category ADD position INT DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE category DROP position');
    }
}
