<?php

namespace App\Repository;

use App\Entity\SalePrice;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method SalePrice|null find($id, $lockMode = null, $lockVersion = null)
 * @method SalePrice|null findOneBy(array $criteria, array $orderBy = null)
 * @method SalePrice[]    findAll()
 * @method SalePrice[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SalePriceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SalePrice::class);
    }

    // /**
    //  * @return SalePrice[] Returns an array of SalePrice objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?SalePrice
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
