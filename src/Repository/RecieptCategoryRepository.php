<?php

namespace App\Repository;

use App\Entity\RecieptCategory;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method RecieptCategory|null find($id, $lockMode = null, $lockVersion = null)
 * @method RecieptCategory|null findOneBy(array $criteria, array $orderBy = null)
 * @method RecieptCategory[]    findAll()
 * @method RecieptCategory[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RecieptCategoryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RecieptCategory::class);
    }

    // /**
    //  * @return RecieptCategory[] Returns an array of RecieptCategory objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?RecieptCategory
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
