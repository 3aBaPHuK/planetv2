<?php

namespace App\Repository;

use App\Entity\DeliveryField;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method DeliveryField|null find($id, $lockMode = null, $lockVersion = null)
 * @method DeliveryField|null findOneBy(array $criteria, array $orderBy = null)
 * @method DeliveryField[]    findAll()
 * @method DeliveryField[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DeliveryFieldRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DeliveryField::class);
    }

    // /**
    //  * @return DeliveryField[] Returns an array of DeliveryField objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?DeliveryField
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
