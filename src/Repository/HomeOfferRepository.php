<?php

namespace App\Repository;

use App\Entity\HomeOffer;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method HomeOffer|null find($id, $lockMode = null, $lockVersion = null)
 * @method HomeOffer|null findOneBy(array $criteria, array $orderBy = null)
 * @method HomeOffer[]    findAll()
 * @method HomeOffer[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class HomeOfferRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, HomeOffer::class);
    }

    // /**
    //  * @return HomeOffer[] Returns an array of HomeOffer objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('h')
            ->andWhere('h.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('h.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?HomeOffer
    {
        return $this->createQueryBuilder('h')
            ->andWhere('h.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
