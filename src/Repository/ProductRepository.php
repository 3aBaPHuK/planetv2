<?php

namespace App\Repository;

use App\Entity\Product;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Product|null find($id, $lockMode = null, $lockVersion = null)
 * @method Product|null findOneBy(array $criteria, array $orderBy = null)
 * @method Product[]    findAll()
 * @method Product[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductRepository extends ServiceEntityRepository
{

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Product::class);
    }

    public function findByOfferId($offerId): ?Product
    {
        $qb = $this->createQueryBuilder('p');

        $res = $qb->select()
            ->andWhere($qb->expr()->like('p.offerCodes', "'%{$offerId}%'"))
            ->getQuery()
            ->getResult();

        return $res && count($res) ? $res[0] : null;
    }

    public function findByCategoryIdsList(array $categoryIds): array
    {
        $qb = $this->createQueryBuilder('p');
        $qb->select()
            ->where('p.category IN (:categoryIds)')
            ->setParameters(['categoryIds' => $categoryIds]);

        return $qb->getQuery()->getArrayResult();
    }

    // /**
    //  * @return Product[] Returns an array of Product objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Product
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
