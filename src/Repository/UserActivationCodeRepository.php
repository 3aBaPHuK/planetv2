<?php

namespace App\Repository;

use App\Entity\UserActivationCode;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method UserActivationCode|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserActivationCode|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserActivationCode[]    findAll()
 * @method UserActivationCode[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserActivationCodeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UserActivationCode::class);
    }

    // /**
    //  * @return UserActivationCode[] Returns an array of UserActivationCode objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?UserActivationCode
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
