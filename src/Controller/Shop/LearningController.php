<?php
/**
 * Created by Elnikov.A
 * User: elnikov.a
 * Date: 15.08.2020
 * Time: 19:44
 */

namespace App\Controller\Shop;

use App\Entity\Lesson;
use Symfony\Component\Routing\Annotation\Route;

class LearningController extends ShopController
{

    /**
     * @Route("/learning/list", name="menu_learning_list")
     */
    public function learningListAction()
    {
        $lessonRepo = $this->getDoctrine()->getRepository(Lesson::class);
        $lessons    = $lessonRepo->findAll();

        return $this->render('shop/learning/list.html.twig', [
            'lessons'   => $lessons ?? [],
            'imagePath' => $this->getParameter('app.path.article_images')
        ]);
    }
}