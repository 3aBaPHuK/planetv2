<?php
/**
 * Created by Elnikov.A
 * User: elnikov.a
 * Date: 23.05.2020
 * Time: 17:48
 */

namespace App\Controller\Shop;

use App\Entity\Cart;
use App\Entity\CartItem;
use App\Entity\Lesson;
use App\Entity\Offer;
use App\Entity\Product;
use App\Entity\SalePrice;
use App\Entity\User;
use App\Traits\CartTrait;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

/**
 * Class CatalogController
 * @Route(name="cart_")
 *
 */
class CartController extends ShopController
{

    /**
     * @Route(path="cart", name="show")
     * @param Request $request
     *
     * @return Response
     */
    public function cartShowAction(Request $request)
    {
        $productRepository = $this->getDoctrine()->getRepository(Product::class);
        $offerRepository   = $this->getDoctrine()->getRepository(Offer::class);
        $cart              = $this->getCurrentUserCart($request);

        if (!$cart) {
            $this->addFlash('notice', 'SHOP_YOUR_CART_IS_EMPTY');

            return $this->redirectToRoute('menu_index');
        }

        $cartItems = $cart->getCartItems()->getValues();
        $products  = [];

        foreach ($cartItems as $cartItem) {
            /**
             * @var CartItem $cartItem
             */

            if (!$cartItem->getOfferId()) {
                continue;
            }

            $offer = $offerRepository->find($cartItem->getOfferId());

            if ($offer && $offer->getFake()) {
                $repo   = $this->getDoctrine()->getRepository($offer->getFromEntity());
                $entity = $repo->find($offer->getFromEntityId());

                $product = [
                    'announce' => $entity->getDescription(),
                    'images'   => [
                        [
                            'id'    => $entity->getId(),
                            'image' => $entity->getImage()
                        ]
                    ],
                    'fake'     => true
                ];
            } elseif ($offer) {
                $product = $productRepository->findByOfferId($offer->getExternalCode());
            } else {
                $this->removeCartItem($cartItem);

                continue;
            }

            $products[$cartItem->getId()] = [
                'product'  => $product,
                'cartItem' => $cartItem,
                'offer'    => $offer
            ];
        }

        return $this->render('shop/cart/show.html.twig', [
            'cart'             => $cart,
            'products'         => $products,
            'total'            => $this->getCartTotal($cart),
            'imagePath'        => $this->getParameter('app.path.product_images'),
            'articleImagePath' => $this->getParameter('app.path.article_images')
        ]);
    }

    private function removeCartItem($cartItem)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($cartItem);
        $em->flush();
    }

    /**
     * @Route(path="cart/item/update", name="item_update")
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function updateCartItem(Request $request)
    {
        $em            = $this->getDoctrine()->getManager();
        $cartItemJson  = $request->get('cartItem');
        $delete        = $request->get('delete', false);
        $cartItemArray = json_decode($cartItemJson, true);
        $cartItemRepo  = $this->getDoctrine()->getRepository(CartItem::class);
        $cartItem      = $cartItemRepo->find($cartItemArray['id']);

        if ($delete) {
            /**
             * @var CartItem $cartItem
             */

            $em->remove($cartItem);
            $em->flush();

            return $this->json([
                'cartItem' => null,
                'removed'  => true,
                'total'    => $this->getCartTotal($cartItem->getCart())
            ]);
        }

        $cartItem->setAmount($cartItemArray['amount']);

        $em->persist($cartItem);
        $em->flush();
        $total = $this->getCartTotal($cartItem->getCart());

        return $this->json(['cartItem' => $cartItemArray, 'total' => $total]);
    }

    /**
     * @Route(path="item/delete/{id}", name="item_delete")
     * @param Request $request
     * @param int $id
     *
     * @return JsonResponse
     */
    public function deleteCartItem(Request $request, int $id)
    {
        $em        = $this->getDoctrine()->getManager();
        $cart      = $this->getCurrentUserCart($request);
        $cartItems = $cart->getCartItems()->getValues();

        $filteredCartItems = array_filter($cartItems, function ($item) use ($id) {
            /**
             * @var CartItem $item
             */
            return $item->getId() === $id;
        });

        if ($filteredCartItems) {
            $cartItem = array_shift($filteredCartItems);

            $cart->removeCartItem($cartItem);

            $em->persist($cart);
            $em->flush();

            return $this->json([
                'success' => 1
            ]);
        }

        return $this->json([
            'error' => 'Item not exist'
        ]);
    }

    /**
     * @Route(path="cart/count", name="count")
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function cartItemsCount(Request $request)
    {
        $cart = $this->getCurrentUserCart($request);

        return $this->json(['itemsCount' => count($cart->getCartItems()->getValues())]);
    }

    /**
     * @Route(path="cart/add/lesson", name="add_lesson")
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function addLessonToCart(Request $request)
    {
        $em         = $this->getDoctrine()->getManager();
        $lessonId   = $request->get('lessonId', null);
        $lessonRepo = $this->getDoctrine()->getRepository(Lesson::class);
        $lesson     = $lessonRepo->find($lessonId);

        if (!$lessonId || !$lesson) {
            return $this->json([
                'error' => 'no lesson'
            ]);
        }

        /**
         * @var User $user
         */
        $user = $this->getUser();
        $cart = $this->getCurrentUserCart($request);

        if (!$cart) {
            $cart = new Cart();

            if ($user) {
                $cart->setCartUser($user);
            }
        }

        $price = new SalePrice();
        $price->setValue($lesson->getPrice());
        $price->setPriceType('Цена по умолчанию');

        $lessonOffer = new Offer();
        $lessonOffer->setName($lesson->getName());
        $lessonOffer->addSalePrice($price);
        $lessonOffer->setQuantity(1);
        $lessonOffer->setFake(true);
        $lessonOffer->setFromEntity('App\\Entity\\Lesson');
        $lessonOffer->setFromEntityId($lesson->getId());

        $em->persist($lessonOffer);
        $em->flush();

        $cartItem = new CartItem();
        $cartItem->setAmount(1);
        $cartItem->setCart($cart);
        $cartItem->setOfferId($lessonOffer->getId());

        $em->persist($cartItem);
        $em->flush();

        return $this->json(['cartId' => $cart->getId()]);
    }

    /**
     * @Route(path="cart/add", name="add")
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function addToCart(Request $request)
    {
        $em              = $this->getDoctrine()->getManager();
        $offerId         = $request->get('offerId');
        $amount          = $request->get('amount', 1);
        $offerRepository = $this->getDoctrine()->getRepository(Offer::class);
        $user            = $this->getUser();

        /**
         * @var Offer $offer
         */
        $offer = $offerRepository->findOneBy(['id' => $offerId]);

        /**
         * @var User $user
         */
        $cart = $this->getCurrentUserCart($request);

        if (!$cart) {
            $cart = new Cart();

            if ($user) {
                $cart->setCartUser($user);
            }
        }

        $cartItem = $this->createOrUpdateCartItem($offer, $cart, $amount);

        $cart->addCartItem($cartItem);

        $em->persist($cart);
        $em->flush();

        return $this->json(['cartId' => $cart->getId()]);
    }

    /**
     * @Route(path="cart/addList", name="add_list")
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function addListToCart(Request $request)
    {
        $em              = $this->getDoctrine()->getManager();
        $offerIdsString  = $request->get('offerIds');
        $offerIds        = json_decode($offerIdsString);
        $offerRepository = $this->getDoctrine()->getRepository(Offer::class);
        $user            = $this->getUser();

        $offers = $offerRepository->findBy(['id' => $offerIds], ['id' => 'DESC']);

        /**
         * @var User $user
         */
        $cart = $this->getCurrentUserCart($request);

        if (!$cart) {
            $cart = new Cart();

            if ($user) {
                $cart->setCartUser($user);
            }
        }

        foreach ($offers as $offer) {
            /**
             * @var Offer $offer
             */
            $cartItem = $this->createOrUpdateCartItem($offer, $cart, 1);
            $cart->addCartItem($cartItem);
        }

        $em->persist($cart);
        $em->flush();

        return $this->json(['cartId' => $cart->getId()]);
    }

    private function createOrUpdateCartItem(?Offer $offer, ?Cart $cart, ?int $amount): CartItem
    {
        $cartItems = $cart->getCartItems()->getValues();

        if ($cartItems) {
            /**
             * @var CartItem[] $existedCartItem
             */
            $existedCartItem = array_filter($cartItems, function ($cartItem) use ($offer) {
                /**
                 * @var CartItem $cartItem
                 */
                return $cartItem->getOfferId() === $offer->getId();
            });

            if ($existedCartItem) {
                $cartItem = array_shift($existedCartItem);
                $cartItem->setAmount($cartItem->getAmount() + $amount);

                return $cartItem;
            }
        }

        $cartItem = new CartItem();
        $cartItem->setAmount($amount);
        $cartItem->setOfferId($offer->getId());
        $cartItem->setCart($cart);

        return $cartItem;
    }

    /**
     * @param Request $request
     *
     * @return Cart|object|null
     */
    private function getCurrentUserCart(Request $request)
    {
        $cartRepository = $this->getDoctrine()->getRepository(Cart::class);

        /**
         * @var User $user
         */
        $user = $this->getUser();

        if ($user) {
            $cart = $user->getUserCart();

            if (!$cart) {
                $cart = new Cart();

                $user->setUserCart($cart);
            }

        } else {
            $cartId = $request->cookies->get('cartId');
            $cart   = $cartId ? $cartRepository->find($cartId) : null;
        }

        return $cart;
    }
}