<?php

namespace App\Controller\Shop;

use App\Entity\Cart;
use App\Entity\Category;
use App\Entity\Menu;
use App\Entity\Offer;
use App\Entity\Shop;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * Created by Elnikov.A
 * User: elnikov.a
 * Date: 25.04.2020
 * Time: 19:09
 */
class ShopController extends AbstractController
{
    /**
     * @var Serializer
     */
    protected $serializer;

    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }

    /**
     * TODO: implements this
     */
    protected function getCurrentShop()
    {
        $shops = $this->getDoctrine()->getRepository(Shop::class)->findAll();

        return array_shift($shops);
    }

    protected function getMenu()
    {
        $er = $this->getDoctrine()->getRepository(Menu::class);

        return $er->findAll();
    }

    protected function getCategories(): array
    {
        $er = $this->getDoctrine()->getRepository(Category::class);
        $categoriesList = $er->findBy([], ['position' => 'ASC']);
        $tree = [];

        $firstLevelCategories = array_filter($categoriesList, function (Category $item) {
            return $item->getLevel() === 1;
        });

        foreach ($firstLevelCategories as $root) {
            $children = $this->createTree($categoriesList, $root);

            $tree[] = [
                'category' => $root,
                'children' => $children
            ];
        }

        return $tree;
    }

    protected function createTree(array $categories, $parent): array
    {
        $children = [];

        usort($categories, function (Category $a, Category $b) {
            return $a->getPosition() - $b->getPosition();
        });

        foreach ($categories as $category) {

            /**
             * @var Category $category
             * @var Category $parent
             */
            if ($category->getParentId() === $parent->getId()) {
                $children[] = [
                    'category' => $category,
                    'children' => $this->createTree($categories, $category)
                ];
            }
        }

        return $children;
    }

    protected function getCart(): ?Cart
    {
        $er   = $this->getDoctrine()->getRepository(Cart::class);
        $em   = $this->getDoctrine()->getManager();
        $user = $this->getUser();

        if ($user) {
            $cart = $user->getUserCart();

            if (!$cart) {
                $cartId = !empty($_COOKIE['cartId']) ? $_COOKIE['cartId'] : null;

                /**
                 * @var Cart | null $cart
                 */
                $cart = $cartId ? $er->find($cartId) : null;

                if ($cart) {
                    $user->setUserCart($cart);
                    $em->persist($user);
                    $em->flush();
                }
            }
        } else {
            $cartId = !empty($_COOKIE['cartId']) ? $_COOKIE['cartId'] : null;

            /**
             * @var Cart $cart
             */
            $cart = $cartId ? $er->find($cartId) : null;
        }

        return $cart;
    }

    protected function getCartTotal(Cart $cart)
    {
        $offerRepo = $this->getDoctrine()->getRepository(Offer::class);
        $total     = 0;
        $cartItems = $cart->getCartItems()->getValues();

        foreach ($cartItems as $cartItem) {
            if (!$cartItem->getOfferId()) {
                continue;
            }

            $offer = $offerRepo->find($cartItem->getOfferId());

            if ($offer) {
                $total += ($offer->getSalePrices()->first()->getValue()) * $cartItem->getAmount();
            }
        }

        return $total;
    }

    protected function render(string $view, array $parameters = [], Response $response = null): Response
    {
        $parameters['menu']        = $this->getMenu();
        $parameters['categories']  = $this->getCategories();
        $parameters['cart']        = $this->getCart();
        $parameters['currentShop'] = $this->getCurrentShop();

        return parent::render($view, $parameters, $response);
    }
}
