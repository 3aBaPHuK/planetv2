<?php

namespace App\Controller\Shop;

use App\Entity\Article;
use App\Entity\Banner;
use App\Entity\HomeOffer;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Created by Elnikov.A
 * User: elnikov.a
 * Date: 23.04.2020
 * Time: 20:27
 */

/**
 * Class HomeController
 * @Route(name="menu_")
 *
 * @package App\Controller
 */
class HomeController extends ShopController
{

    /**
     * @Route("/", name="index")
     *
     * @return Response
     */
    public function indexAction()
    {
        $banners          = $this->getDoctrine()->getRepository(Banner::class)->findAll();
        $homeOffers       = $this->getDoctrine()->getRepository(HomeOffer::class)->findAll();
        $homeNews         = $this->getDoctrine()->getRepository(Article::class)->getLatestArticles(3);
        $bannerImagesPath = $this->getParameter('app.path.banner_images');
        $newsImagePath    = $this->getParameter('app.path.article_images');

        return $this->render('shop/common/index.html.twig', [
            'banners'          => $banners,
            'homeOffers'       => $homeOffers,
            'homeNews'         => $homeNews,
            'bannerImagesPath' => $bannerImagesPath,
            'newsImagePath'    => $newsImagePath
        ]);
    }
}