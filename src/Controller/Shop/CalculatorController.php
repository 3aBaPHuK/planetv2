<?php
/**
 * Created by Elnikov.A
 * User: elnikov.a
 * Date: 15.08.2020
 * Time: 19:50
 */

namespace App\Controller\Shop;


use Symfony\Component\Routing\Annotation\Route;

/**
 * Class CalculatorController
 *
 * @package App\Controller\Shop
 */

class CalculatorController extends ShopController
{

    /**
     * @Route("/calculator", name="menu_calculator")
     */
    public function showAction() {

        return $this->render('shop/calculator/show.html.twig');
    }
}