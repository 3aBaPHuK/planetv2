<?php
/**
 * Created by Elnikov.A
 * User: elnikov.a
 * Date: 27.04.2020
 * Time: 20:19
 */

namespace App\Controller\Shop;

use App\Entity\Category;
use App\Entity\Offer;
use App\Entity\Product;
use App\Entity\ProductOffer;
use App\Form\Shop\Catalog\ProductList\SortType;
use App\Repository\ProductRepository;
use App\Service\Breadcrumbs\Breadcrumbs;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * Class CatalogController
 * @Route(name="catalog_")
 *
 * @package App\Controller\Shop
 */
class CatalogController extends ShopController
{
    /**
     * @var Breadcrumbs
     */
    private $breadcrumbs;

    public function __construct(SerializerInterface $serializer, Breadcrumbs $breadcrumbs)
    {
        $this->breadcrumbs = $breadcrumbs;

        parent::__construct($serializer);
    }

    /**
     * @Route(path="catalog/{categorySlug}/{productSlug}", name="product")
     * @param string $productSlug
     *
     * @return Response
     */
    public function productAction(string $productSlug): Response
    {
        $productRepo = $this->getDoctrine()->getRepository(Product::class);

        /**
         * @var Product $product
         */
        $product       = $productRepo->findOneBy(['slug' => $productSlug]);
        $productOffers = $product->getProductOffers()->toArray();
        $inStock       = 0;
        $offers        = [];

        usort($productOffers, function (ProductOffer $a, ProductOffer $b) {
            return (int)$a->getDisplayName() - (int)$b->getDisplayName();
        });

        if (count($productOffers)) {
            foreach ($productOffers as $productOffer) {
                $offer    = $productOffer->getOffer();
                $offers[] = $offer;
            }
        }

        foreach ($offers as $offer) {
            $inStock += $offer->getStock();
        }

        $productPrices = [];

        foreach ($offers as $offer) {
            $productPrices[$product->getId()][$offer->getId()] = $offer->getSalePrices()->toArray()[0];
        }

        return $this->render('shop/catalog/product/show.html.twig', [
            'product'       => $product,
            'productOffers' => $productOffers,
            'offers'        => $offers,
            'productPrices' => $productPrices,
            'inStock'       => $inStock,
            'imagePath'     => $this->getParameter('app.path.product_images'),
            'breadcrumbs'   => $this->breadcrumbs->getBreadcrumbCollection()
        ]);
    }


    /**
     * @param string $slug
     * @param Request $request
     *
     * @return Response
     * @Route(path="catalog/{slug}", name="list")
     */
    public function productListAction(string $slug, Request $request)
    {
        $limit = $request->get('limit', 6);
        $page  = $request->get('page', 0);

        $sort = $request->get('sort', [
            'orderBy' => 'name_DESC'
        ]);

        $sortForm = $this->createForm(SortType::class);
        $sortForm->setData($sort);

        $categoryRepo  = $this->getDoctrine()->getRepository(Category::class);
        $allCategories = $categoryRepo->findAllSort(['name' => 'ASC']);

        /**
         * @var Category $currentCategory
         * @var Category $mainCategory
         */
        $currentCategory = $categoryRepo->findOneBy(['slug' => $slug]);
        $mainCategory    = $this->getMainCategory($currentCategory, $allCategories);
        $categories      = $categoryRepo->findBy(['parentId' => $mainCategory->getId()], ['name' => 'ASC']);
        $tree            = $this->getCategoryTree($categories, $allCategories);

        [$products, $totalProductsCount] = $this->getCategoryProducts($currentCategory, $sort, $page, $limit);

        $childrenCategories = array_filter($allCategories, function ($cat) use ($currentCategory) {
            /**
             * @var Category $cat
             */
            return $cat->getParentId() === $currentCategory->getId();
        });

        $offers        = $this->getProductsOffers($products);
        $productPrices = [];

        foreach ($offers as $productId => $productOffers) {
            if (!isset($productOffers['offers'])) {
                continue;
            }

            foreach ($productOffers['offers'] as $offer) {
                $productPrices[$productId][$offer->getId()] = $offer->getSalePrices()->toArray()[0];
            }
        }

        return $this->render('shop/catalog/productList/show.html.twig', [
            'sort'               => $sort,
            'pages'              => intdiv($totalProductsCount, $limit),
            'page'               => $page,
            'limit'              => $limit,
            'tree'               => $tree,
            'sortForm'           => $sortForm->createView(),
            'products'           => $products,
            'offers'             => $offers,
            'productPrices'      => $productPrices,
            'category'           => $currentCategory,
            'childrenCategories' => $this->getCategoryTree($childrenCategories, $allCategories),
            'imagePath'          => $this->getParameter('app.path.product_images')
        ]);
    }

    /**
     * @param Product[] $products
     *
     * @return array
     */
    private function getProductsOffers($products)
    {
        $offers = [];

        foreach ($products as $product) {
            $productOffers = $product->getProductOffers()->toArray();

            usort($productOffers, function (ProductOffer $a, ProductOffer $b) {
                return (int)$a->getDisplayName() - (int)$b->getDisplayName();
            });

            $offers[$product->getId()]['productOffers'] = $productOffers;

            foreach ($productOffers as $productOffer) {
                $offers[$product->getId()]['offers'][] = $productOffer->getOffer();
            }
        }

        return $offers;
    }

    /**
     * @param Category $currentCategory
     * @param          $sort
     * @param          $page
     * @param          $limit
     *
     * @return array
     */
    private function getCategoryProducts(Category $currentCategory, $sort, $page, $limit): array
    {
        $orderBy     = explode('_', $sort['orderBy']);
        $productRepo = $this->getDoctrine()->getRepository(Product::class);
        $productQuery = $productRepo->createQueryBuilder('p');
        $productQuery->where('p.category = ' . $currentCategory->getId());
        $productQuery->addOrderBy('p.' . $orderBy[0], $orderBy[1]);
        $productQuery->addOrderBy('p.offerCodes', 'DESC');

        if ($searchString = !empty($sort['search']) ? '%' . $sort['search'] . '%' : false) {
            $productQuery->setParameter('search', $searchString);
            $productQuery->andWhere('p.name LIKE :search');
        }

        $products           = $productQuery->getQuery()->getResult();
        $totalProductsCount = count($products);

        $products = array_slice($products, $page * $limit, $limit);

        return [$products, $totalProductsCount];
    }

    /**
     * @param Category $category
     * @param array $allCategories
     *
     * @return Category|mixed
     */
    private function getMainCategory(Category $category, array $allCategories)
    {
        if ($category->getLevel() !== 1) {
            while ($category->getLevel() !== 1) {
                foreach ($allCategories as $cat) {
                    if ($cat->getId() === $category->getParentId()) {
                        $category = $cat;
                    }
                }
            }

        }

        return $category;
    }

    /**
     * @param array $categories
     * @param array $allCategories
     *
     * @return array
     */
    private function getCategoryTree(array $categories, array $allCategories): array
    {
        $tree = [];

        usort($categories, function (Category $a, Category $b) {
            return $a->getPosition() - $b->getPosition();
        });

        foreach ($categories as $node) {
            $tree[] = [
                'children' => $this->createTree($allCategories, $node),
                'category' => $node,
            ];
        }

        return $tree;
    }
}
