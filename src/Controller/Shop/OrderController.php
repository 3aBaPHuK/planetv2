<?php
/**
 * Created by Elnikov.A
 * User: elnikov.a
 * Date: 19.07.2020
 * Time: 17:57
 */

namespace App\Controller\Shop;

use App\Entity\Cart;
use App\Entity\CartItem;
use App\Entity\Delivery;
use App\Entity\DeliveryField;
use App\Entity\Offer;
use App\Entity\Order;
use App\Entity\OrderItem;
use App\Entity\OrderStatus;
use App\Entity\Payment;
use App\Entity\Product;
use App\Message\OrderCreatedEmail;
use App\Service\CDEKService;
use DateTime;
use Exception;
use InvalidArgumentException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGenerator;
use Symfony\Contracts\HttpClient\Exception\ExceptionInterface;

/**
 * Class CatalogController
 * @Route(name="order_")
 *
 */
class OrderController extends ShopController
{

    /**
     * @Route(path="order/create", name="create")
     * @param Request     $request
     *
     * @param CDEKService $CDEKService
     *
     * @return Response
     */
    public function createOrderAction(Request $request, CDEKService $CDEKService)
    {
        $cart         = $this->getCart();
        $deliveryRepo = $this->getDoctrine()->getRepository(Delivery::class);
        $paymentRepo  = $this->getDoctrine()->getRepository(Payment::class);
        $deliveries   = $deliveryRepo->findAll();
        $payments     = $paymentRepo->getEnabledPayments();
        $deliveryData = $this->formatDeliveries($CDEKService, $deliveries);

        try {
            list($products, $prices, $totalWeight) = $this->formatCartItems($cart);
        } catch (InvalidArgumentException $exception) {
            return $this->redirectToRoute('menu_index');
        }

        return $this->render('shop/order/create.html.twig', [
            'products'        => $products,
            'cartTotal'       => $this->getCartTotal($cart),
            'prices'          => $prices,
            'deliveries'      => $deliveryData,
            'payments'        => $payments,
            'paymentIconPath' => $this->getParameter('app.path.payment_icons'),
            'totalWeight'     => $totalWeight
        ]);
    }


    /**
     * @Route(path="order/invoice", name="invoice")
     * @param Request $request
     *
     * @return Response
     */
    public function showInvoiceAction(Request $request)
    {
        $orderId = $request->get('order_id');
        $shop    = $this->getCurrentShop();

        if (!$orderId) {
            $this->redirectToRoute('menu_index');
        }

        /**
         * @var Order $order
         */
        $order = $this->getDoctrine()->getRepository(Order::class)->find($orderId);

        if ((int)$order->getPayment()->getType() !== Payment::PAYMENT_INVOICE_TYPE) {

            return $this->redirectToRoute('menu_index');
        }

        $orderTotal = 0;

        foreach ($order->getOrderItems() as $orderItem) {
            $orderTotal += ($orderItem->getPrice() * $orderItem->getAmount());
        }

        return $this->render('shop/payment/invoiceTemplate.html.twig', ['order' => $order, 'shop' => $shop, 'orderTotal' => $orderTotal]);
    }

    /**
     * @Route(path="order/process", name="process")
     * @param Request             $request
     *
     * @param MessageBusInterface $messageBus
     *
     * @return Response
     */
    public function processOrderAction(Request $request, MessageBusInterface $messageBus)
    {
        $orderData      = $request->get('order');
        $orderDataArray = json_decode($orderData, true);

        try {
            $newOrder = $this->createOrder($orderDataArray);

            if ($newOrder) {
                $em = $this->getDoctrine()->getManager();

                $em->remove($this->getCart());
                $em->flush();

                $messageBus->dispatch(new OrderCreatedEmail($newOrder->getId(), $this->generateUrl('menu_index', [], UrlGenerator::ABSOLUTE_URL), $this->getCurrentShop()));
            }

            return $this->json(['success' => 1, 'order_id' => $newOrder->getId()]);
        } catch (Exception $e) {

            return $this->json(['error' => $e->getMessage()]);
        }
    }

    /**
     * @Route(path="order/complete", name="complete")
     * @param Request $request
     *
     * @return Response
     */
    public function completeOrderAction(Request $request)
    {
        $orderId = $request->get('order_id');

        if (!$orderId) {
            $this->redirectToRoute('menu_index');
        }

        /**
         * @var Order $order
         */
        $order = $this->getDoctrine()->getRepository(Order::class)->find($orderId);

        if ((int)$order->getPayment()->getType() === Payment::PAYMENT_INVOICE_TYPE) {

            return $this->render('shop/order/completeWithInvoice.html.twig', [
                'order' => $order
            ]);
        }

        return $this->render('shop/order/complete.html.twig', [
            'order' => $order
        ]);
    }

    /**
     * @param array $orderData
     *
     * @return Order
     * @throws Exception
     */
    private function createOrder(array $orderData): Order
    {
        $newOrder           = new Order();
        $orderStatusRepo    = $this->getDoctrine()->getRepository(OrderStatus::class);
        $deliveryRepo       = $this->getDoctrine()->getRepository(Delivery::class);
        $paymentRepo        = $this->getDoctrine()->getRepository(Payment::class);
        $cart               = $this->getCart();
        $orderDelivery      = $deliveryRepo->find($orderData['deliveryId']);
        $orderPayment       = $paymentRepo->find($orderData['paymentId']);
        $orderItemsIterator = $this->createOrderItemsFromCart($cart);

        $newOrder->setCreatedAt(new DateTime('now'));
        $newOrder->setUpdatedAt(new DateTime('now'));
        $newOrder->setStatus($orderStatusRepo->getInitialStatus());
        $newOrder->setDelivery($orderDelivery);
        $newOrder->setPayment($orderPayment);
        $newOrder->setOrderUser($this->getCart()->getCartUser());

        if (!empty($orderData['buyer'])) {
            $newOrder->setBuyerInfo($orderData['buyer']);
        }

        foreach ($orderItemsIterator as $orderItem) {
            $newOrder->addOrderItem($orderItem);
        }

        $em = $this->getDoctrine()->getManager();

        $em->persist($newOrder);
        $em->flush();

        return $newOrder;
    }

    /**
     * @param Cart $cart
     *
     * @return \Generator
     */
    private function createOrderItemsFromCart(Cart $cart): \Generator
    {
        $offerRepository = $this->getDoctrine()->getRepository(Offer::class);

        foreach ($cart->getCartItems() as $cartItem) {
            if (!$cartItem->getOfferId()) {
                continue;
            }

            /**
             * @var Offer $offer
             */
            $offer = $offerRepository->find($cartItem->getOfferId());

            if (!$offer) {
                continue;
            }

            $price = $offer->getSalePrices()->first();

            if (!$price) {
                continue;
            }

            $newOrderItem = new OrderItem();
            $newOrderItem->setName($offer->getName());
            $newOrderItem->setAmount($cartItem->getAmount());
            $newOrderItem->setPrice($price->getValue());
            $newOrderItem->setWeight($offer->getWeight());

            yield $newOrderItem;
        }
    }

    /**
     * @param CDEKService $CDEKService
     * @param array       $deliveries
     *
     * @return array
     */
    private function formatDeliveries(CDEKService $CDEKService, array $deliveries): array
    {
        $deliveryData = [];

        foreach ($deliveries as $key => $delivery) {
            $deliveryData[] = [
                'id'          => $delivery->getId(),
                'description' => $delivery->getDescription(),
                'fields'      => $delivery->getFields()->getValues(),
                'icon'        => $this->getParameter('app.path.delivery_type_icons') . DIRECTORY_SEPARATOR
                    . $delivery->getType()->getIcon(),
                'name'        => $delivery->getType()->getName()
            ];

            foreach ($deliveryData[$key]['fields'] as $field) {
                if ($field->getType() === DeliveryField::DELIVERY_FIELD_CDEK_ADDRESS) {
                    $request            = $CDEKService->getDeliveryPoints(['country_codes' => 'RU']);
                    try {
                        $data               = $request->query();
                        $CDEKDeliveryPoints = array_shift($data);

                        $field->setValues($CDEKDeliveryPoints);
                    } catch (ExceptionInterface $exception) {

                    }
                }
            }
        }

        return $deliveryData;
    }

    /**
     * @param Cart|null $cart
     *
     * @return array
     */
    private function formatCartItems(?Cart $cart): array
    {
        $prices            = [];
        $products          = [];
        $offerRepository   = $this->getDoctrine()->getRepository(Offer::class);
        $productRepository = $this->getDoctrine()->getRepository(Product::class);
        $totalWeight       = 0;

        if (!$cart) {
            throw new InvalidArgumentException('Cart is empty');
        }

        foreach ($cart->getCartItems() as $cartItem) {
            /**
             * @var CartItem $cartItem
             */

            if (!$cartItem->getOfferId()) {
                continue;
            }

            /**
             * @var Offer $offer
             */
            $offer = $offerRepository->find($cartItem->getOfferId());

            if (!$offer) {

                continue;
            }

            $totalWeight += ($offer->getWeight() * $cartItem->getAmount());

            $products[$cartItem->getId()] = [
                'product'  => $productRepository->findByOfferId($offer->getExternalCode()),
                'cartItem' => $cartItem,
                'offer'    => $offer
            ];

            $prices[$cartItem->getId()] = $offer->getSalePrices()->first()->getValue();
        }

        return [$products, $prices, $totalWeight];
    }
}
