<?php
/**
 * Created by Elnikov.A
 * User: elnikov.a
 * Date: 26.04.2020
 * Time: 16:10
 */

namespace App\Controller\Shop;

use App\Entity\User;
use App\Entity\UserActivationCode;
use App\Form\Shop\UserRegistrationType;
use App\Message\NewUserWelcomeEmail;
use DateTime;
use Exception;
use LogicException;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Generator\UrlGenerator;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class UserController extends ShopController
{

    /**
     * @Route("/register", name="user_registration")
     * @param Request                      $request
     * @param UserPasswordEncoderInterface $passwordEncoder
     *
     * @param MessageBusInterface          $messageBus
     *
     * @return RedirectResponse|Response
     * @throws Exception
     */
    public function registerAction(
        Request $request, UserPasswordEncoderInterface $passwordEncoder, MessageBusInterface $messageBus
    ) {
        $user = new User();
        $form = $this->createForm(UserRegistrationType::class, $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $existingUser = $this->getDoctrine()->getRepository(User::class)->findBy(['login' => $user->getLogin()]);

            if ($existingUser) {
                $this->addFlash('notice', 'USER_ALREADY_EXIST');

                return $this->redirectToRoute('user_registration');
            }

            $user->setPassword($passwordEncoder->encodePassword($user, $user->getPassword()));
            $user->setActive(false);

            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            $this->addFlash('success', 'YOU_ARE_REGISTERED_PLEASE_ACTIVATE_ACCOUNT');

            $userActivationEntity = $this->getDoctrine()->getRepository(UserActivationCode::class);

            /**
             * @var UserActivationCode $activationHash
             */
            $activationHash = $userActivationEntity->findBy(['activationUser' => $user->getId()]);

            if (!$activationHash) {
                $activationHash = new UserActivationCode();
                $activationHash->setActivationUser($user);
                $activationHash->setHash(md5((new DateTime())->format('H:i') . $user->getId() . $user->getLogin()));

                $em->persist($activationHash);
                $em->flush();
            }

            $activationUrl = $this->generateUrl('user_activate', ['activation_code' => $activationHash->getHash()], UrlGenerator::ABSOLUTE_URL);
            $baseUrl       = $this->generateUrl('menu_index', [], UrlGenerator::ABSOLUTE_URL);
            $shop          = $this->getCurrentShop();

            $messageBus->dispatch(new NewUserWelcomeEmail($user->getId(), $activationUrl, $baseUrl, $shop));

            return $this->redirectToRoute('menu_index');
        }

        return $this->render(
            'shop/user/registration.html.twig',
            ['form' => $form->createView()]
        );
    }

    /**
     * @Route("/login", name="user_login")
     * @param AuthenticationUtils $authenticationUtils
     *
     * @param Request             $request
     *
     * @return Response
     */
    public function login(AuthenticationUtils $authenticationUtils, Request $request): Response
    {
        if ($this->getUser()) {
            $sfRedirectString = $request->cookies->get('sf_redirect');

            if ($sfRedirectString) {
                $sfRedirect = json_decode($sfRedirectString, true);
                $route      = $sfRedirect['route'];
            }

            return $this->redirectToRoute($route ?? 'menu_index');
        }

        $error        = $authenticationUtils->getLastAuthenticationError();
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('shop/user/login.html.twig', ['last_username' => $lastUsername, 'error' => $error]);
    }

    /**
     * @Route("/logout", name="user_logout")
     */
    public function logout()
    {
        throw new LogicException('This method can be blank - it will be intercepted by the logout key on your firewall.');
    }

    /**
     * @Route("/activate", name="user_activate")
     * @param Request $request
     *
     * @return RedirectResponse
     */
    public function activateUser(Request $request)
    {
        $hash = $request->get('activation_code');

        if (!$hash) {
            $this->redirect('/');
        }

        $userActivationCodeRepo = $this->getDoctrine()->getRepository(UserActivationCode::class);

        /**
         * @var $activationCode UserActivationCode
         */
        $activationCode = $userActivationCodeRepo->findOneBy(['hash' => $hash]);

        if (!$activationCode) {
            $this->redirect('/');
        }

        $user = $activationCode->getActivationUser();

        if (!$user) {
            $this->redirect('/');
        }

        $user->setActive(true);

        $em = $this->getDoctrine()->getManager();

        $em->persist($user);
        $em->flush();

        $this->addFlash('success', 'WELCOME_TO_SHOP_ALERT');

        return $this->redirectToRoute('user_login');
    }
}