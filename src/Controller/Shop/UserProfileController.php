<?php
/**
 * Created by Elnikov.A
 * User: elnikov.a
 * Date: 04.05.2020
 * Time: 21:37
 */

namespace App\Controller\Shop;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

/**
 * Class UserProfileController
 *@Route(name="profile_")
 *
 * @package App\Controller\Shop
 */
class UserProfileController extends ShopController
{

    /**
     * @Route(name="index", path="/profile")
     */
    public function indexAction(AuthorizationCheckerInterface $authorizationChecker) {

        return $this->render('shop/profile/show.html.twig');
    }
}