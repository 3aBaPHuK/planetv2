<?php
/**
 * Created by Elnikov.A
 * User: elnikov.a
 * Date: 15.08.2020
 * Time: 19:53
 */

namespace App\Controller\Shop;

use App\Entity\Article;
use App\Entity\ArticleCategory;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class InformationController extends ShopController
{

    /**
     * @Route(path="/information", name="menu_information")
     * @param Request $request
     *
     * @return Response
     */
    public function showInfoPage(Request $request)
    {
        $articleId = $request->get('id');

        if (!$articleId) {
            return $this->redirect('/');
        }

        $articleRepo = $this->getDoctrine()->getRepository(Article::class);

        $article = $articleRepo->find($articleId);

        if (!$article) {
            return $this->redirect('/');
        }

        return $this->render('shop/information/show.html.twig', [
            'article'   => $article,
            'imagePath' => $this->getParameter('app.path.article_images')
        ]);
    }

    /**
     * @Route(path="/information/list", name="menu_information_list")
     * @param Request $request
     *
     * @return Response
     */
    public function showInfoList(Request $request)
    {
        $articleCategories = $this->getDoctrine()->getRepository(ArticleCategory::class)->findAll();
        $currentCategoryId = $request->get('category_id');
        $currentArticleId  = $request->get('article_id');
        $currentCategory   = null;
        $currentArticle    = null;

        if (!$articleCategories) {
            return $this->redirect('/');
        }

        if ($currentCategoryId) {
            $currentCategory = $this->getDoctrine()->getRepository(ArticleCategory::class)->find($currentCategoryId);
        }

        if ($currentArticleId) {
            $currentArticle = $this->getDoctrine()->getRepository(Article::class)->find($currentArticleId);
        }

        return $this->render('shop/information/list.html.twig', [
            'currentCategory'   => $currentCategory,
            'currentArticle'    => $currentArticle,
            'articleCategories' => $this->sortArticleByUpdatedAt($articleCategories),
            'imagePath'         => $this->getParameter('app.path.article_images')
        ]);
    }

    private function sortArticleByUpdatedAt(array $arr) {
        $res = $arr;

        usort($arr, function (ArticleCategory $a, ArticleCategory $b) {
            return $a->getUpdatedAt() > $b->getUpdatedAt();
        });

        return $res;
    }
}