<?php
/**
 * Created by Elnikov.A
 * User: elnikov.a
 * Date: 15.08.2020
 * Time: 19:47
 */

namespace App\Controller\Shop;

use App\Entity\Reciept;
use App\Entity\RecieptCategory;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class Recipe extends ShopController
{

    /**
     * @Route ("recipe/list", name="menu_recipe_list")
     * @param Request $request
     *
     * @return Response
     */
    public function recipeListAction(Request $request)
    {
        $categoryId           = $request->get('category', null);
        $currentCategory      = null;
        $recipeRepo           = $this->getDoctrine()->getRepository(Reciept::class);
        $recipeCategoriesRepo = $this->getDoctrine()->getRepository(RecieptCategory::class);

        if ($categoryId) {

            /**
             * @var RecieptCategory $currentCategory
             */
            $currentCategory = $recipeCategoriesRepo->find($categoryId);
        }

        $recipes = $currentCategory ? $currentCategory->getReciepts() : $recipeRepo->findAll();

        return $this->render('shop/recipe/list.html.twig', [
            'recipes'          => $recipes,
            'recipeCategory'   => $currentCategory,
            'recipeCategories' => $recipeCategoriesRepo->findAll(),
            'imagePath'        => $this->getParameter('app.path.article_images')
        ]);
    }

    /**
     * @Route ("recipe", name="recipe")
     * @param Request $request
     *
     * @return Response
     */
    public function showRecipeAction(Request $request)
    {
        $recipeId             = $request->get('recipe', null);
        $recipeRepo           = $this->getDoctrine()->getRepository(Reciept::class);
        $recipeCategoriesRepo = $this->getDoctrine()->getRepository(RecieptCategory::class);

        $recipe = $recipeRepo->find($recipeId);

        return $this->render('shop/recipe/show.html.twig', [
            'recipe'           => $recipe,
            'recipeId'         => $recipeId,
            'recipeCategories' => $recipeCategoriesRepo->findAll(),
            'imagePath'        => $this->getParameter('app.path.article_images')
        ]);
    }
}