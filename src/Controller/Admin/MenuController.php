<?php
/**
 * Created by Elnikov.A
 * User: elnikov.a
 * Date: 25.04.2020
 * Time: 17:49
 */

namespace App\Controller\Admin;

use App\Form\Admin\MenuParamsType;
use EasyCorp\Bundle\EasyAdminBundle\Controller\EasyAdminController;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

class MenuController extends EasyAdminController
{

    protected function createEntityFormBuilder($entity, $view)
    {
        /**
         * @var Router $router
         */
        $router     = $this->container->get('router');
        $collection = $router->getRouteCollection();
        $allRoutes  = $collection->all();

        $routes = array_filter($allRoutes, function ($route) {
            return strpos($route, 'menu_') !== false;
        }, ARRAY_FILTER_USE_KEY);

        $choices = [];

        foreach ($routes as $key => $route) {
            $choices[$route->getPath()] = $key;
        }

        $formBuilder = parent::createEntityFormBuilder($entity, $view);

        $categoryChoices[''] = null;

        $formBuilder
            ->add('route', ChoiceType::class, [
                'choices' => $choices,
            ])->add('params', CollectionType::class, [
                'entry_type' => MenuParamsType::class,
                'allow_add'=> true,
                'allow_delete'=> true,
                'delete_empty'=> true,
            ]);

        return $formBuilder;
    }
}