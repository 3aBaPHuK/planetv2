<?php
/**
 * Created by Elnikov.A
 * User: elnikov.a
 * Date: 26.04.2020
 * Time: 14:27
 */

namespace App\Controller\Admin;

use App\Entity\Offer;
use App\Form\Admin\OfferEmbeddedForm;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use EasyCorp\Bundle\EasyAdminBundle\Controller\EasyAdminController;
use EasyCorp\Bundle\EasyAdminBundle\Event\EasyAdminEvents;
use EasyCorp\Bundle\EasyAdminBundle\Exception\EntityRemoveException;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;

class ProductController extends EasyAdminController
{

    protected function deleteAction()
    {
        $forceDelete = $this->request->get('force', false);
        $this->dispatch(EasyAdminEvents::PRE_DELETE);

        if ('DELETE' !== $this->request->getMethod() && !$forceDelete) {
            return $this->redirect($this->generateUrl('easyadmin', [
                'action' => 'list',
                'entity' => $this->entity['name']
            ]));
        }

        $id   = $this->request->query->get('id');
        $form = $this->createDeleteForm($this->entity['name'], $id);
        $form->handleRequest($this->request);

        if ($form->isSubmitted() && $form->isValid() || $forceDelete) {
            $easyadmin = $this->request->attributes->get('easyadmin');
            $entity    = $easyadmin['item'];

            $this->dispatch(EasyAdminEvents::PRE_REMOVE, ['entity' => $entity]);

            try {
                $this->executeDynamicMethod('remove<EntityName>Entity', [$entity, $form]);
            } catch (ForeignKeyConstraintViolationException $e) {
                throw new EntityRemoveException([
                    'entity_name' => $this->entity['name'],
                    'message'     => $e->getMessage()
                ]);
            }

            $this->dispatch(EasyAdminEvents::POST_REMOVE, ['entity' => $entity]);
        }

        $this->dispatch(EasyAdminEvents::POST_DELETE);

        return $this->redirectToReferrer();
    }
}