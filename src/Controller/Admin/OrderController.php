<?php
/**
 * Created by Laximo.
 * User: elnikov.a
 * Date: 28.11.2020
 * Time: 20:13
 */

namespace App\Controller\Admin;

use App\Entity\Shop;
use EasyCorp\Bundle\EasyAdminBundle\Controller\EasyAdminController;
use App\Message\OrderStatusChangedEmail;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Generator\UrlGenerator;

class OrderController extends EasyAdminController
{

    private $messageBus;

    public function __construct(MessageBusInterface $messageBus)
    {
        $this->messageBus = $messageBus;
    }

    public function updateEntity($entity)
    {

        parent::updateEntity($entity);

        $shops = $this->getDoctrine()->getRepository(Shop::class)->findAll();
        $shop  = array_shift($shops);

        $this->messageBus->dispatch(new OrderStatusChangedEmail($entity->getId(), $this->generateUrl('menu_index', [], UrlGenerator::ABSOLUTE_URL), $shop));

    }
}