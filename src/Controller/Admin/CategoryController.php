<?php
/**
 * Created by Elnikov.A
 * User: elnikov.a
 * Date: 12.04.2020
 * Time: 0:38
 */

namespace App\Controller\Admin;

use App\Entity\Category;
use App\Entity\Currency;
use App\Entity\Image;
use App\Entity\Product;
use App\Entity\ProductImage;
use App\Entity\ProductProperties;
use App\Form\Admin\import\ImportFormType;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use EasyCorp\Bundle\EasyAdminBundle\Controller\EasyAdminController;
use EasyCorp\Bundle\EasyAdminBundle\Event\EasyAdminEvents;
use EasyCorp\Bundle\EasyAdminBundle\Exception\EntityRemoveException;
use Generator;
use laacz\XLSParser\Book;
use laacz\XLSParser\XLSParserException;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class CategoryController extends EasyAdminController
{

    public function thirdStepAction()
    {
        $columnNums   = $this->request->get('mapping');
        $fileName     = $this->request->get('fileName');
        $skipFirstRow = $this->request->get('skip_first', false);

        $path = $this->getParameter('app.path.imports') . DIRECTORY_SEPARATOR . $fileName;
        try {
            $iterator = $this->readTheFile($path);
            $rowSkipped = false;

            foreach ($iterator as $row) {
                if ($skipFirstRow && !$rowSkipped) {
                    $rowSkipped = true;

                    continue;
                }

                if (!$row) {
                    continue;
                }

                $product           = new Product();
                $productProperties = new ProductProperties();
                $categoriesData = [];

                foreach ($columnNums as $columnNum => $columnName) {

                    switch ($columnName) {
                        case 'additionalImages':
                            $image = $this->createAdditionalImage($row[$columnNum]);

                            if ($image) {
                                $productProperties->addAdditionalImage($image);
                            }

                            break;
                        case 'article':
                        case 'bonus':
                        case 'instructions':
                        case 'parameters':
                        case 'examples':
                        case 'size':
                        case 'rating':
                        case 'reciepts':
                            $capitalizePropName = ucfirst($columnName);
                            $productProperties->{"set{$capitalizePropName}"}($row[$columnNum]);

                            break;
                        case 'name':
                        case 'announce':
                        case 'description':
                            $capitalizePropName = ucfirst($columnName);

                            $product->{"set{$capitalizePropName}"}($row[$columnNum]);
                            break;
                        case 'image':
                            $imageCollection = $this->createImage($row[$columnNum], false);

                            if ($imageCollection) {
                                $product->addImageCollection($imageCollection);
                            }
                            break;
                        case 'image_main':
                            $imageCollection = $this->createImage($row[$columnNum], true);

                            if ($imageCollection) {
                                $product->addImageCollection($imageCollection);
                            }
                            break;
                        case 'category1':
                        case 'category2':
                        case 'category3':
                            $level = (int)str_replace('category', '', $columnName);

                            $path = $this->createCategoryPath($row, $columnNums, $level);

                            if ($level) {
                                $categoriesData[$level] = [
                                    'name' => $row[$columnNum],
                                    'path' => $path
                                ];
                            }
                            break;
                    }
                }

                $product->setCategory($this->createCategory($categoriesData));
                $product->setProductProperties($productProperties);

                $currencyRepo = $this->em->getRepository(Currency::class);

                /**
                 * @var $firstCurrency Currency
                 */
                $firstCurrency = $currencyRepo->findOneBy([]);

                if (!$product->getQuantity()) {
                    $product->setQuantity(0);
                }

                if (!$product->getPrice()) {
                    $product->setPrice(0);
                }

                $product->setCurrency($firstCurrency);
                $this->em->persist($product);
                $this->em->flush();
            }

        } catch (XLSParserException $e) {
        }

        return $this->renderTemplate('thirdStep', $this->entity['templates']['third_step'], []);
    }

    /**
     * @param $path
     *
     * @return Generator
     */
    private function readTheFile(string $path): Generator
    {
        $handle = fopen($path, "r");

        while (!feof($handle)) {
            yield fgetcsv($handle, 0, ';');
        }

        fclose($handle);
    }

    public function secondStepAction()
    {
        $form = $this->createForm(ImportFormType::class);
        $form->handleRequest($this->request);

        /**
         * @var UploadedFile $priceListFile
         */
        $priceListFile = $form->get('file')->getData();

        if ($priceListFile) {
            try {
                $path = $this->getParameter('app.path.imports') . DIRECTORY_SEPARATOR
                    . $priceListFile->getClientOriginalName();
                $priceListFile->move($this->getParameter('app.path.imports'), $priceListFile->getClientOriginalName());

                $parameters = [
                    'rows'             => $this->getTestData($path),
                    'fileName'         => $priceListFile->getClientOriginalName(),
                    'availableColumns' => array_merge([''], Product::IMPORTED_PROPERTIES)
                ];

                return $this->renderTemplate('secondStep', $this->entity['templates']['second_step'], $parameters);
            } catch (FileException $e) {
            } catch (XLSParserException $e) {
            }
        }
    }

    private function getTestData($path) {
        $rows = [];

        $iterator = $this->readTheFile($path);

        while ($iterator->key() < 5) {
            $rows[] = $iterator->current();

            $iterator->next();
        }

        return $rows;
    }

    public function importAction()
    {
        $form       = $this->createForm(ImportFormType::class);
        $parameters = [
            'form' => $form->createView()
        ];

        return $this->renderTemplate('import', $this->entity['templates']['import'], $parameters);
    }

    public function createCategoryEntityFormBuilder($entity, $view)
    {
        /**
         * @var Category $entity
         */

        $formBuilder = parent::createEntityFormBuilder($entity, $view);

        $er         = $this->em->getRepository('App\Entity\Category');
        $categories = $er->findAll();

        $categoryChoices = [];

        foreach ($categories as $category) {
            $categoryChoices[$category->getPath()] = $category->getId();
        };

        $categoryChoices[''] = null;

        $formBuilder->add('parentId', ChoiceType::class, [
            'choices' => $categoryChoices
        ]);

        return $formBuilder;
    }

    protected function deleteAction()
    {
        $forceDelete = $this->request->get('force', false);
        $this->dispatch(EasyAdminEvents::PRE_DELETE);

        if ('DELETE' !== $this->request->getMethod() && !$forceDelete) {
            return $this->redirect($this->generateUrl('easyadmin', [
                'action' => 'list',
                'entity' => $this->entity['name']
            ]));
        }

        $id   = $this->request->query->get('id');
        $form = $this->createDeleteForm($this->entity['name'], $id);
        $form->handleRequest($this->request);

        if ($form->isSubmitted() && $form->isValid() || $forceDelete) {
            $easyadmin = $this->request->attributes->get('easyadmin');
            $entity    = $easyadmin['item'];

            $this->dispatch(EasyAdminEvents::PRE_REMOVE, ['entity' => $entity]);

            try {
                $this->executeDynamicMethod('remove<EntityName>Entity', [$entity, $form]);
            } catch (ForeignKeyConstraintViolationException $e) {
                throw new EntityRemoveException([
                    'entity_name' => $this->entity['name'],
                    'message'     => $e->getMessage()
                ]);
            }

            $this->dispatch(EasyAdminEvents::POST_REMOVE, ['entity' => $entity]);
        }

        $this->dispatch(EasyAdminEvents::POST_DELETE);

        return $this->redirectToReferrer();
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response|void
     */
    protected function listAction()
    {
        $this->dispatch(EasyAdminEvents::PRE_LIST);

        $fields    = $this->entity['list']['fields'];
        $paginator = $this->findAll($this->entity['class'], $this->request->query->get('page', 1), $this->entity['list']['max_results'], $this->request->query->get('sortField'), $this->request->query->get('sortDirection'), $this->entity['list']['dql_filter']);

        $this->dispatch(EasyAdminEvents::POST_LIST, ['paginator' => $paginator]);

        $er          = $this->em->getRepository(Category::class);
        $productRepo = $this->em->getRepository(Product::class);

        $categories    = $er->findAll();

        usort($categories, function (Category $a, Category $b) {
            return $a->getPosition() - $b->getPosition();
        });

        $nodesNoParent = array_filter($categories, function ($category) {
            return !$category->getParentId();
        });

        $tree          = [];

        foreach ($nodesNoParent as $node) {
            $tree[] = [
                'children' => $this->createTree($categories, $node),
                'category' => $node,
                'products' => $productRepo->findBy(['category' => $node])
            ];
        }

        $parameters = [
            'paginator'            => $paginator,
            'tree'                 => $tree,
            'fields'               => $fields,
            'batch_form'           => $this->createBatchForm($this->entity['name'])->createView(),
            'delete_form_template' => $this->createDeleteForm($this->entity['name'], '__id__')->createView(),
        ];

        return $this->executeDynamicMethod('render<EntityName>Template', [
            'list',
            $this->entity['templates']['list'],
            $parameters
        ]);
    }

    /**
     * @param Category $entity
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    protected function persistEntity($entity)
    {
        $entity->setLevel($this->createLevel($entity));
        $entity->setPath($this->createEntityCategoryPath($entity));

        $this->em->persist($entity);
        $this->em->flush();
    }

    /**
     * @param Category $entity
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    protected function updateEntity($entity)
    {
        $entity->setLevel($this->createLevel($entity));
        $entity->setPath($this->createEntityCategoryPath($entity));

        $this->em->persist($entity);
        $this->em->flush();
    }


    private function createEntityCategoryPath(Category $entity) {
        $path = $entity->getName();

        if ($entity->getParentId()) {
            $er = $this->em->getRepository(Category::class);
            $category = $entity;

            while ($category && ($parentId = $category->getParentId())) {
                $category = $er->findOneBy(['id' => $parentId]);

                $path = $category->getName() . '/' . $path;
            }
        }

        return $path;
    }

    private function createCategoryPath($row, $columnNums, $level)
    {
        $path = '';

        if (($category1 = array_search('category1', $columnNums)) && $level >= 1) {
            $path .= $row[$category1];
        }

        if (($category2 = array_search('category2', $columnNums)) && $level >= 2) {
            $path .= '/' . $row[$category2];
        }

        if (($category3 = array_search('category3', $columnNums)) && $level >= 3) {
            $path .= '/' . $row[$category3];
        }

        return $path;
    }

    private function createCategory($categoriesData)
    {
        $er = $this->em->getRepository(Category::class);

        $categoriesData = array_filter($categoriesData, function ($item) {
            return $item;
        });

        $level = max(array_keys($categoriesData));
        $name  = $categoriesData[$level]['name'];
        $path  = $categoriesData[$level]['path'];

        $category = $er->findOneBy(['path' => $path]);

        if ($category) {
            return $category;
        }

        $newCategory = new Category();
        $newCategory->setUpdatedAt(new \DateTime('now'));
        $newCategory->setLevel($level);
        $newCategory->setStatus(true);
        $newCategory->setName($name);
        $newCategory->setPath($path);
        $newCategory->setParentId($this->getOrCreateParentCategory($categoriesData, $level - 1));

        $this->em->persist($newCategory);
        $this->em->flush();

        return $newCategory;
    }

    private function getOrCreateParentCategory($categoriesData, $level)
    {
        $parentId = null;
        $er       = $this->em->getRepository(Category::class);

        if ($level > 0) {

            $name = $categoriesData[$level]['name'];
            $path = $categoriesData[$level]['path'];

            $parentCategory = $er->findOneBy(['path' => $path]);

            if (!$parentCategory) {

                $parentCategory = new Category();
                $parentCategory->setUpdatedAt(new \DateTime('now'));
                $parentCategory->setLevel($level);
                $parentCategory->setStatus(true);
                $parentCategory->setName($name);
                $parentCategory->setPath($path);
                $parentCategory->setParentId($this->getOrCreateParentCategory($categoriesData, $level - 1));

                $this->em->persist($parentCategory);
                $this->em->flush();
            }

            $parentId = $parentCategory->getId();
        }

        return $parentId;
    }

    private function createAdditionalImage($url) {
        $fileServer = $this->getParameter('app.path.imports.imageserver');
        $fileUrl    = $fileServer . $url;

        try {
            $fileContent       = file_get_contents($fileUrl);
            $productImage      = new Image();
            $productImagesPath = $this->getParameter('app.path.product_images');
            $rootPath          = $this->getParameter('kernel.project_dir');
            $newFileUrl        = str_replace('/', '_', $url);
            file_put_contents($rootPath . '/public' . $productImagesPath . '/' . $newFileUrl, $fileContent);

            $productImage->setImage($newFileUrl);
            $productImage->setName($newFileUrl);
            $productImage->setUpdatedAt(new \DateTime('now'));

            return $productImage;
        } catch (\Exception $exception) {
            return null;
        }
    }

    private function createImage($url, $main)
    {
        $fileServer = $this->getParameter('app.path.imports.imageserver');
        $fileUrl    = $fileServer . $url;

        try {
            $fileContent       = file_get_contents($fileUrl);
            $productImage      = new ProductImage();
            $productImagesPath = $this->getParameter('app.path.product_images');
            $rootPath          = $this->getParameter('kernel.project_dir');
            $newFileUrl        = str_replace('/', '_', $url);
            file_put_contents($rootPath . '/public' . $productImagesPath . '/' . $newFileUrl, $fileContent);

            $productImage->setImage($newFileUrl);
            $productImage->setMain($main);
            $productImage->setUpdatedAt(new \DateTime('now'));

            return $productImage;
        } catch (\Exception $exception) {
            return null;
        }
    }

    private function createTree(array $categories, $parent)
    {
        $children = [];

        foreach ($categories as $category) {

            /**
             * @var Category $category
             * @var Category $parent
             */
            if ($category->getParentId() === $parent->getId()) {
                $child = [
                    'category' => $category,
                    'children' => $this->createTree($categories, $category)
                ];

                $productRepo       = $this->em->getRepository(Product::class);
                $child['products'] = $productRepo->findBy(['category' => $category]);

                $children[] = $child;

            }
        }

        return $children;
    }

    private function createLevel($entity)
    {
        $level = 1;

        if ($entity->getParentId()) {
            $er             = $this->em->getRepository('App\Entity\Category');
            $parentCategory = $er->find($entity->getParentId());

            $level = $parentCategory->getLevel() + 1;
        }

        return $level;
    }
}
