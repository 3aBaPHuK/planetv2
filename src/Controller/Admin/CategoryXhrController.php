<?php

namespace App\Controller\Admin;

use App\Repository\CategoryRepository;
use App\Repository\ProductRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route(name="admin_catalog_")
 *
 * @package App\Controller\Shop
 */
class CategoryXhrController
{
    /**
     * @var ProductRepository
     */
    private $productRepository;
    /**
     * @var CategoryRepository
     */
    private $categoryRepository;

    public function __construct(ProductRepository $productRepository, CategoryRepository $categoryRepository)
    {
        $this->productRepository = $productRepository;
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * @Route(path="admin/category/products", name="products_by_category_ids", methods={"POST"})
     *
     * @param Request $request
     * @return Response
     */
    public function getProductListByCategoryIds(Request $request): Response
    {
        $idsString = $request->getContent();
        $categoryIds = explode(',', $idsString, 30);

        $products = $this->productRepository->findByCategoryIdsList($categoryIds);

        return new JsonResponse($products);
    }

    /**
     * @Route(path="admin/category/position/update", name="update_category_position", methods={"POST"})
     *
     * @param Request $request
     * @return Response
     */
    public function updateCategoryTreePosition(Request $request): Response
    {
        $bodyString = $request->getContent();
        $body = json_decode($bodyString, true);

        $this->categoryRepository->updatePositions($body);


        return new JsonResponse();
    }
}
