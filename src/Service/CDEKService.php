<?php
/**
 * Created by Elnikov.A
 * User: elnikov.a
 * Date: 25.07.2020
 * Time: 17:31
 */

namespace App\Service;

use Psr\Log\LoggerInterface;
use Symfony\Component\HttpClient\CurlHttpClient;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class CDEKService extends WebService
{
    public function getDeliveryPoints($params) {
        $this->createRequest('GET', 'deliverypoints', [], $params);

        return $this;
    }

    public function getCities($params) {
        $this->createRequest('GET', 'location/cities');

        return $this;
    }

    public function getAuthToken()
    {
        $client = new CurlHttpClient();

        $query = http_build_query([
            'grant_type'    => 'client_credentials',
            'client_id'     => $this->parameters->get('app.CDEK.api.login'),
            'client_secret' => $this->parameters->get('app.CDEK.api.password')
        ]);

        $url = $this->parameters->get('app.CDEK.api.url') . 'oauth/token?' . $query;

        $response = $client->request('POST', $url)->getContent();

        $data = json_decode($response, true);

        if (empty($data['access_token'])) {
            return null;
        }

        return $data['access_token'];
    }

    public function query($single = false): array
    {
        $client              = new CurlHttpClient();
        $responses           = [];
        $this->requestTexts  = [];
        $this->responseTexts = [];
        $token = $this->getAuthToken();

        if (!$this->requests) {
            return [];
        }

        foreach ($this->requests as $request) {
            $url = $this->parameters->get('app.CDEK.api.url') . $request->getUrl();

            if ($params = $request->getParams()) {
                $url .= '?' . http_build_query($params);
            }

            $options = [
                'auth_bearer' => $token,
            ];

            if ($request->getMethod() !== 'GET') {
                $options['json'] = $request->getBody();
            }

            try {
                $curlResponse = $client->request($request->getMethod(), $url, $options);

                $responseBody = $curlResponse->getContent();

                $this->requestTexts[]  = $this->createTextRequest($request);
                $this->responseTexts[] = $responseBody;

                $responses[] = json_decode($responseBody);
            } catch (TransportExceptionInterface $e) {
            }

        }

        $this->requests = [];

        return $responses;
    }
}
