<?php

namespace App\Service;


class Request
{

    /**
     * @var string
     */
    private $method;

    /**
     * @var string
     */
    private $command;

    /**
     * @var array
     */
    private $body;

    /**
     * @var array|null
     */
    private $params = null;

    /**
     * @var string
     */
    private $path = '';

    /**
     * @param string $method
     * @param string $command
     * @param array  $body
     * @param array|null   $params
     *
     * @return $this
     */
    public function createRequest(string $method, string $command, array $body, $params = null) {
        $instance = new self();

        $instance->setMethod($method);
        $instance->setCommand($command);
        $instance->setBody($body);
        $instance->setParams($params);

        return $instance;
    }

    public function getUrl() {
        return $this->path . $this->getCommand();
    }

    /**
     * @return array
     */
    public function getBody(): array
    {
        return $this->body;
    }

    /**
     * @return string
     */
    public function getMethod(): string
    {
        return $this->method;
    }

    /**
     * @return string
     */
    public function getCommand():string
    {
        return $this->command;
    }

    /**
     * @param string $command
     *
     */
    public function setCommand($command)
    {
        $this->command = $command;
    }

    /**
     * @param array $body
     *
     */
    public function setBody(array $body)
    {
        $this->body = $body;
    }

    /**
     * @param string $method
     *
     */
    public function setMethod(string $method)
    {
        $this->method = $method;
    }

    /**
     * @return string
     */
    public function getPath(): string
    {
        return $this->path;
    }

    /**
     * @param string $path
     */
    public function setPath(string $path): void
    {
        $this->path = $path;
    }

    /**
     * @return array|null
     */
    public function getParams(): ?array
    {
        return $this->params;
    }

    /**
     * @param array|null $params
     */
    public function setParams(?array $params): void
    {
        $this->params = $params;
    }
}