<?php


namespace App\Service\Breadcrumbs;


use App\Entity\Category;
use App\Entity\Product;
use App\Service\Breadcrumbs\Object\Breadcrumb;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpFoundation\RequestStack;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Routing\Generator\UrlGenerator;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class Breadcrumbs
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var Product $product
     */
    private $product;

    /**
     * @var ArrayCollection
     */
    private $breadcrumbCollection;

    /**
     * @var UrlGeneratorInterface
     */
    private $router;
    /**
     * @var \Symfony\Component\HttpFoundation\Request|null
     */
    private $request;

    public function __construct(EntityManagerInterface $entityManager, UrlGeneratorInterface $router, RequestStack $requestStack)
    {
        $this->entityManager = $entityManager;
        $this->router        = $router;
        $this->request       = $requestStack->getCurrentRequest();
        $pathInfo            = $this->request->getPathInfo();
        $pathChunks          = explode('/', $pathInfo);
        $productSlug         = array_pop($pathChunks);

        if (!$productSlug || !$product = $this->entityManager->getRepository(Product::class)->findOneBy(['slug' => $productSlug])) {

            return;
        }
        $this->product = $product;

        $this->createProductBreadcrumbs();
    }

    public function createProductBreadcrumbs(): void
    {
        $path = $this->getCurrentCategoryTree();

        if (!$this->breadcrumbCollection) {
            $this->breadcrumbCollection = new ArrayCollection();

            foreach ($path as $category) {
                $categoryLink = $this->router->generate('catalog_list', ['slug' => $category->getSlug()], UrlGenerator::ABSOLUTE_URL);

                $breadcrumb = new Breadcrumb($category->getName(), $categoryLink);
                $this->breadcrumbCollection->add($breadcrumb);
            }
        }
    }

    /**
     * @return array
     */
    private function getCurrentCategoryTree(): array
    {
        $firstCategory = $this->product->getCategory();
        $categoryRepo  = $this->entityManager->getRepository(Category::class);
        $categories    = [];
        $category      = $firstCategory;
        $categories[]  = $firstCategory;

        while ($category->getParentId()) {
            $category     = $categoryRepo->find($category->getParentId());
            $categories[] = $category;
        }


        return array_reverse($categories);
    }

    public function getBreadcrumbCollection(): ArrayCollection
    {
        return $this->breadcrumbCollection;
    }

    /**
     * @param Product $product
     */
    public function setProduct(Product $product): void
    {
        $this->product = $product;
    }
}