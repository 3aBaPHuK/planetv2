<?php
namespace App\Service\Breadcrumbs\Object;

class Breadcrumb
{
    /**
     * @var string
     */
    private $title;

    /**
     * @var string $url
     */
    private $link;

    public function __construct(string $title, string $link)
    {
        $this->title = $title;
        $this->link  = $link;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getLink(): string
    {
        return $this->link;
    }
}