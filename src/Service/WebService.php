<?php

namespace App\Service;

use App\Service\Request;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpClient\CurlHttpClient;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class WebService
{

    /**
     * @var string
     */
    protected $baseUrl = '';

    /**
     * @var array
     */
    protected $authorizationData = [];

    protected $error;

    /**
     * @var string
     */
    protected $method;

    /**
     * @var array
     */
    protected $requestTexts;

    /**
     * @var array
     */
    protected $responseTexts;

    /**
     * @var array
     */
    protected $requests;

    /**
     * @var \App\Service\Request
     */
    protected $request;

    /**
     * @var ParameterBagInterface
     */
    protected $parameters;

    public function __construct(ParameterBagInterface $parameters)
    {
        $this->parameters = $parameters;
        $this->request    = new Request();
    }

    /**
     * @param string $method
     * @param string $command
     * @param array  $body
     * @param null   $params
     */
    public function createRequest(string $method, string $command, array $body = [], $params = null): void
    {
        $this->requests[] = $this->request->createRequest($method, $command, $body, $params);
    }

    public function query($single = false)
    {
        $client              = new CurlHttpClient();
        $responses           = [];
        $this->requestTexts  = [];
        $this->responseTexts = [];
        $this->getAuthorizationData();

        if (!$this->requests) {
            return [];
        }

        foreach ($this->requests as $request) {
            $url = $this->getBaseUrl() . $request->getUrl();

            $options = [];

            $options = [
                'auth' => $this->getAuthorizationData(),
                'json' => $request->getBody()
            ];

            try {
                $curlResponse = $client->request($request->getMethod(), $url, $options);

                $responseBody = $curlResponse->getContent();

                $this->requestTexts[]  = $this->createTextRequest($request);
                $this->responseTexts[] = $responseBody;

                $aa = get_class();


//                if ($single) {
//                    return $factory->createObject($request->getCommand(), $responseBody);
//                }
//
//                $responses[] = $factory->createObject($request->getCommand(), $responseBody);
            } catch (TransportExceptionInterface $e) {
            }

        }

        $this->requests = [];

        return $responses;
    }

    /**
     * @return mixed
     */
    public function getBaseUrl()
    {
        return $this->baseUrl;
    }

    /**
     * @param mixed $baseUrl
     */
    public function setBaseUrl($baseUrl): void
    {
        $this->baseUrl = $baseUrl;
    }

    /**
     * @return mixed
     */
    public function getAuthorizationData()
    {
        return $this->authorizationData;
    }

    /**
     * @param mixed $authorizationData
     */
    public function setAuthorizationData($authorizationData): void
    {
        $this->authorizationData = $authorizationData;
    }

    /**
     * @return mixed
     */
    public function getMethod()
    {
        return $this->method;
    }

    /**
     * @param mixed $method
     */
    public function setMethod($method): void
    {
        $this->method = $method;
    }

    protected function createTextRequest(Request $request)
    {
        $requestTextData = [$request->getMethod(), $request->getUrl()];

        if ($request->getBody()) {
            $requestTextData[] = json_encode($request->getBody(), JSON_PRETTY_PRINT);
        }

        return join(': ', $requestTextData);
    }
}