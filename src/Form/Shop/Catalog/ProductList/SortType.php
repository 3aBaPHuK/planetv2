<?php
/**
 * Created by Elnikov.A
 * User: elnikov.a
 * Date: 04.05.2020
 * Time: 15:09
 */

namespace App\Form\Shop\Catalog\ProductList;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SearchType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SortType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $sortTypes = [
            'BY_NAME_DESC'  => 'name_DESC',
            'BY_NAME_ASC'   => 'name_ASC',
            'BY_PRICE_ASC'  => 'price_ASC',
            'BY_PRICE_DESC' => 'price_DESC',
        ];

        $builder->setMethod('GET');

        $builder->add('orderBy', ChoiceType::class, [
            'label'   => 'CATALOG_SORT_TYPE',
            'choices' => $sortTypes,
        ]);

        $builder->add('search', SearchType::class, [
            'label' => 'CATALOG_SEARCH',
            'required' => false
        ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(['csrf_protection' => false]);
    }
}