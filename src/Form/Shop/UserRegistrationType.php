<?php
/**
 * Created by Elnikov.A
 * User: elnikov.a
 * Date: 26.04.2020
 * Time: 16:10
 */

namespace App\Form\Shop;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class UserRegistrationType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', EmailType::class, ['label' => 'SHOP_REGISTER_EMAIL'])
            ->add('login', TextType::class, ['label' => 'SHOP_REGISTER_LOGIN'])
            ->add('firstName', TextType::class, ['label' => 'SHOP_REGISTER_FIRST_NAME'])
            ->add('lastName', TextType::class, ['label' => 'SHOP_REGISTER_LAST_NAME'])
            ->add('password', RepeatedType::class, [
                'type'           => PasswordType::class,
                'first_options'  => ['label' => 'SHOP_REGISTER_PASSWORD'],
                'second_options' => ['label' => 'SHOP_REGISTER_PASSWORD_REPEAT'],
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'SHOP_REGISTER_USER',
                'attr'  => [
                    'class' => 'btn'
                ]
            ]);
    }
}