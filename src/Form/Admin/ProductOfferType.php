<?php
/**
 * Created by Elnikov.A
 * User: elnikov.a
 * Date: 26.04.2020
 * Time: 22:11
 */

namespace App\Form\Admin;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;

class ProductOfferType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('offer_codes', ChoiceType::class, [
            'choice_label' => 'name',
            'choice_value' => 'externalCode',
        ]);
    }
}