<?php
/**
 * Created by Elnikov.A
 * User: elnikov.a
 * Date: 18.04.2020
 * Time: 16:19
 */

namespace App\Form\Admin;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;

class ImageCollectionFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('image', CollectionType::class, [
            'entry_type' => ImageEmbeddedForm::class,
            'allow_delete' => true,
            'allow_add' => true,
            'by_reference' => false,
        ]);
    }
}