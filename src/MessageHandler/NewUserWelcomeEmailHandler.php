<?php
/**
 * Created by Elnikov.A
 * User: elnikov.a
 * Date: 05.05.2020
 * Time: 17:49
 */

namespace App\MessageHandler;

use App\Message\NewUserWelcomeEmail;
use App\Repository\UserRepository;
use Psr\Log\LoggerInterface;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Component\Mime\Email;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

class NewUserWelcomeEmailHandler implements MessageHandlerInterface
{

    private $userRepository;

    /**
     * @var Environment
     */
    private $twig;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var MailerInterface
     */
    private $mailer;

    public function __construct(
        UserRepository $userRepository, Environment $twig, LoggerInterface $logger, MailerInterface $mailer
    ) {
        $this->userRepository = $userRepository;
        $this->twig           = $twig;
        $this->logger         = $logger;
        $this->mailer         = $mailer;
    }

    public function __invoke(NewUserWelcomeEmail $welcomeEmail)
    {
        $user           = $this->userRepository->find($welcomeEmail->getUserId());
        $activationLink = $welcomeEmail->getUserActivationLink();

        try {
            $this->logger->debug('start message' . PHP_EOL);

            $body = $this->twig->render('email/welcome.html.twig', [
                'user'           => $user,
                'activationLink' => $activationLink,
                'baseUrl'        => $welcomeEmail->getBaseUrl(),
                'shop'           => $welcomeEmail->getShop()
            ]);
            $this->logger->debug('body: ' . PHP_EOL);
            $this->logger->debug($body . PHP_EOL);

            $email = (new Email())
                ->from('duron12@yandex.ru')
                ->to($user->getEmail())
                ->subject('Регистрация "Планета мыла"')
                ->html($body);

            $this->logger->debug('sendRes: ' . $this->mailer->send($email) . PHP_EOL);

        } catch (LoaderError $e) {
            $this->logger->error($e->getMessage());
        } catch (RuntimeError $e) {
            $this->logger->error($e->getMessage());
        } catch (SyntaxError $e) {
            $this->logger->error($e->getMessage());
        } catch (TransportExceptionInterface $e) {
            $this->logger->error($e->getMessage());
        }

    }
}