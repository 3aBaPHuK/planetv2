class AdminTree {
    constructor() {
        const carets      = document.querySelectorAll('.caret');
        const caretsArray = Array.prototype.slice.call(carets);

        caretsArray.forEach(caret => {
            caret.addEventListener('click', ev => {
                this.toggle(ev.target);
            })
        });
    }

    toggle(caret) {
        const childrenContainer = caret.closest('.tree-level');
        const containerId = childrenContainer.dataset.id;

        const elementsToShow = Array.prototype.slice.call(document.querySelectorAll('.table .tree-level[data-parent-id="' + containerId + '"]'));
        const products = childrenContainer.querySelector('.products[data-id="' + containerId + '"]');

        if (products) {
            products.classList.toggle('active');
        }

        elementsToShow.forEach(elem => {
            elem.classList.toggle('active')
        });

        caret.classList.toggle('fa-folder');
        caret.classList.toggle('fa-folder-open');
    }
}