import './extensions/methods';

import Vue from 'vue';
import VTooltip from 'v-tooltip';
import 'es6-promise/auto';
import 'document-register-element/build/document-register-element';
import vueCustomElement from './customElements/src/vue-custom-element';

import './extensions/directives';
import './extensions/filters';

import 'sl-vue-tree/dist/sl-vue-tree-minimal.css'

Vue.use(vueCustomElement);
Vue.use(VTooltip);

import './extensions/bootstrap';

import './extensions/global';
import '../css/categories.css';
