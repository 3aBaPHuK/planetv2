export class UrlExtension {
    setUrlParam(name, value, href) {
        let url = new URL(href);
        let params = (url).searchParams;
        if (value) {
            params.set(name, value);
        } else {
            params.delete(name);
        }
        let query = params.toString();
        let pathname = url.origin + url.pathname;
        return query ? (pathname + '?' + query) : pathname;
    }

    getUrlParam(name, href) {
        if (!href) {
            href = document.location;
        }

        let url = new URL(href);
        let params = (url).searchParams;

        return params.get(name);
    }

    setHistoryUrlParam(name, value) {
        let url = this.setUrlParam(name, value, document.location);
        window.history.replaceState({}, '', url);
    }

    setHistoryUrlParamList(list, prefix, url) {
        url = url ? url : document.location;

        Object.keys(list).forEach(name => {
            if (list[name] !== null && typeof list[name] === 'object') {
                url = this.setHistoryUrlParamList(list[name], `${prefix}[${name}]`, url);
            } else {
                url = this.setUrlParam(`${prefix}[${name}]`, list[name], url);
            }
        });

        window.history.replaceState({}, '', url);

        return url;
    }

    resetHistoryUrlParam(param) {
        let url = new URL(document.location);
        let params = (url).searchParams;
        let toDelete = [];

        for (var key of params.keys()) {

            if (key === param || key.indexOf(`${param}`) === 0) {
                toDelete.push(key);
            }
        }

        toDelete.forEach(key => {
            params.delete(key);
        });

        let query = params.toString();
        let pathname = url.origin + url.pathname;
        let href = query ? (pathname + '?' + query) : pathname;

        window.history.replaceState({}, '', href);
    }

    setItemStore(name, value) {
        localStorage.setItem(name, value);
    }

    getItemStore(name) {
        return localStorage.getItem(name);
    }
}