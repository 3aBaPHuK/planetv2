import Vue from 'vue';
import {Extension} from './Extension';
import Store from '../store/store';
export const EventBus = new Vue();

const req = require.context('../components/', true, /\.(vue)$/i);

const componentFiles = req.keys();
let registeredComponents = [];

document.addEventListener('DOMContentLoaded', () => {
    (function bootstrap(componentFiles, Extension, Store, EventBus) {
        componentFiles.forEach((filepath, key) => {
            let filename = filepath.replace(/^.*[\\\/]/, '');
            let tagName = (filename.charAt(0).toLowerCase() + filename.slice(1)).toCebabCase().replace(/\.(vue)$/g, '');
            let wcTagName = 'wc-' + tagName;
            let vueComponent = req(filepath);


            registeredComponents.push({
                filename, tagName, wcTagName
            });

            Vue.mixin({
                methods: {
                    clickOutsideHandler(event) {
                        if (typeof this.clickOutside !== 'function') {
                            return;
                        }

                        this.clickOutside(event);
                    }
                },
                data() {
                    return {
                        store: {},
                        clickAnywhereRegistered: false
                    };
                },
                created() {
                    this.ext      = new Extension();
                    this.store    = Store;
                    this.eventBus = EventBus;
                },
                mounted() {
                    if (this.clickAnywhereRegistered) {return;}

                    this.$el.addEventListener('click', (ev) => {
                        this.eventBus.$emit('click-anywhere', ev);
                    });

                    this.clickAnywhereRegistered = true;
                }
            });

            let component = vueComponent.default || vueComponent;

            Vue.customElement(wcTagName, component);
        });

        document.dispatchEvent(new CustomEvent('VueComponentsLoaded'));

    })(componentFiles, Extension, Store, EventBus);
});


window.showRegisteredComponents = function () {
    console.log(registeredComponents);
};

window.registeredComponents = registeredComponents;