import Vue from 'vue';
import Inputmask from 'inputmask';

Vue.directive('mask', function (el, binding) {
    if (binding.value) {
        let inputMask = new Inputmask({mask: binding.value, placeholder: ''});
        inputMask.mask(el);
    }
});

Vue.directive('mask-regex', function (el, binding) {
    if (binding.value) {
        let inputMask = new Inputmask({regex: binding.value, placeholder: ''});
        inputMask.mask(el);
    }
});

Vue.directive('mask-regex-placeholder', function (el, binding){
    if (binding.value) {
        let inputMask = new Inputmask({regex: binding.value});
        inputMask.mask(el);
    }
});