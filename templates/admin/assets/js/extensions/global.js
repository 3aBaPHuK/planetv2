document.addEventListener('VueComponentsLoaded', () => {
    initAlerts();
    initBurger();
    initCategoriesControls();
});

function initAlerts() {
    const alerts = Array.from(document.querySelectorAll('.close-alert'));

    alerts.forEach(alert => alert.addEventListener('click', ev => {
        const alert = ev.target.closest('.alert');

        if (alert) {
            alert.remove();
        }
    }))
}

function toggleBurgerIcon(element) {
    if (element.classList.contains('fa-bars')) {
        element.classList.remove('fa-bars');
        element.classList.add('fa-close');

        return;
    }

    element.classList.remove('fa-close');
    element.classList.add('fa-bars');
}

function initBurger() {
    const burgerButton       = document.querySelector('.burger-title');
    const burgerContent      = document.querySelector('.burger-content');
    const closeContentButton = document.querySelector('.close-burger-menu');

    if (burgerButton && burgerContent) {
        burgerButton.addEventListener('click', () => {
            burgerContent.classList.toggle('opened');
            toggleBurgerIcon(burgerButton.querySelector('i'));
        })
    }

    if (closeContentButton && burgerContent) {
        closeContentButton.addEventListener('click', () => {
            burgerContent.classList.remove('opened');
            toggleBurgerIcon(burgerButton.querySelector('i'));
        })
    }
}

window.toggleCategories = function(button) {
    const categoriesBlock = document.querySelector('.categories-tree-sidebar');

    categoriesBlock.classList.toggle('opened');
    const icon = button.querySelector('i');

    if (icon.classList.contains('fa-chevron-down')) {
        icon.classList.remove('fa-chevron-down');
        icon.classList.add('fa-chevron-up');

        return;
    }

    icon.classList.remove('fa-chevron-up');
    icon.classList.add('fa-chevron-down');
};

function initCategoriesControls() {
    const controls = Array.from(document.querySelectorAll('.categories-tree-controls [data-control]'));

    if (controls.length) {
        controls.forEach(button => {
            button.addEventListener('click', () => {
                const methodName = button.dataset.control;
                window[methodName](button);
            })
        });
    }
}