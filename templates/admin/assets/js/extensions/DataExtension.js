export class DataExtension {
    getData(value) {
        return typeof value === 'string' && value.length > 0 ? JSON.parse(value) : value;
    }

    prepareListData(data) {
        data.forEach(item => {
            item.hidden = false;
            item.selected = true;
        });
        return data;
    }

    compareInterval(value, from, to) {

        value = parseFloat(value);
        let toReturn = false;

        if (from && from.length) {
            toReturn = value < parseFloat(from);
        }

        if (toReturn) {
            return toReturn;
        }

        if (from && from.length) {
            toReturn = value > parseFloat(to);
        }

        return toReturn;
    }

    compareNumber(value, filter, action) {
        let toReturn = false;
        if (filter && filter.length) {
            filter = parseFloat(filter);
            value = parseFloat(value);
            action = action ? action.toLowerCase() : 'equal';

            if (action === 'more') {
                toReturn = value <= filter;
            } else if (action === 'less') {
                toReturn = value >= filter;
            } else {
                toReturn = value !== filter;
            }
        }
        return toReturn;
    }

    compareString(value, filter) {
        return filter && filter.length && value.toLowerCase().indexOf(filter.toLowerCase()) === -1;
    }

    equalString(value, filter) {
        return filter && filter.length && String(value).toLowerCase() !== String(filter).toLowerCase();
    }

    inFilterArray(value, filter) {
        return filter && filter.length && filter.indexOf(value) === -1;
    }

    inItemArray(data, filter, field) {
        if (filter === undefined || filter.length === 0) {
            return false;
        }

        if (filter && filter.length && !data) {
            return true;
        }
        filter = Number(filter);
        let filteredData = Object.values(data).filter(item => item[field] === filter);

        return filteredData.length ? false : true;
    }

    equalNumber(value, filter) {

        return filter && filter.length && parseFloat(value) !== parseFloat(filter);
    }

    cloneObject(object) {
        return JSON.parse(JSON.stringify(object));
    }

    currencyFormat(number, ...args) {
        return new Intl.NumberFormat(...args).format(number);
    }
}