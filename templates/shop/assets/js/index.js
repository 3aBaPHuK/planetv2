import './extensions/methods';

import Vue from 'vue';
import VTooltip from 'v-tooltip';
import 'es6-promise/auto';
import 'document-register-element/build/document-register-element';
import vueCustomElement from './customElements/src/vue-custom-element';

import './extensions/directives';
import './extensions/filters';

Vue.use(vueCustomElement);
Vue.use(VTooltip);

import './extensions/bootstrap';

import './extensions/global';

import '../fonts/lato/lato.css';
import '../css/grid.shop.css';
import '../css/base.shop.css';
import '../css/base.modal.css';
import '../css/base.select.css';
import '../css/base.tab.css';
import '../css/cart.shop.css';
import '../css/order.shop.css';
import '../css/form.shop.css';
import '../css/product.shop.css';
import '../css/shop.lesson.css';
import '../css/shop.recipe.css';
import '../css/banner.shop.css';
import '../css/homeOffer.shop.css';
import '../css/shop.article.css';
import '../css/shop.calculator.css';
import '../css/shop.css';
import '../css/tab/grid.shop.css';
import '../css/tab/base.shop.css';
import '../css/tab/shop.css';
import '../css/mob/grid.shop.css';
import '../css/mob/base.shop.css';
import '../css/mob/shop.css';
import '../css/mob/homeOffer.shop.css';
import '../css/mob/banner.shop.css';
