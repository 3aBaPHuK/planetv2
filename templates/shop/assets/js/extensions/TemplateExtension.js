export class TemplateExtension {
    stringToDOM(str){
        let parser = new DOMParser();
        return parser.parseFromString(str, "text/html");
    }

    jsonToArray(str){
        let parser = new DOMParser();
        return parser.parseFromString(str, "text/html");
    }

     getTemplate(template, vars) {
         Object.keys(vars).forEach(variable => {
             this[variable] = vars[variable];
         });
         return eval('`'+ template +'`');
     }

     makeId(length) {
        let result             = '';
        const characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        const charactersLength = characters.length;

        for ( let i = 0; i < length; i++ ) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }

        return result;
    }
}