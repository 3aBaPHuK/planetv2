export class EventExtension {
    observeEvent(type, selector, callback) {
        document.addEventListener(type, (evt) => {
            if (evt.target.matches(selector)) {
                callback(evt);
            }
        }, false);
    }

    emitEvent(emitElement, eventName, value) {

        emitElement.$emit(eventName, value);

        if (emitElement.$parent) {
            this.emitEvent(emitElement.$parent, eventName, value);
        }
    }
}