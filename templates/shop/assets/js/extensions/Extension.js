import {HttpRequestExtension} from './HttpRequestExtension';
import {UrlExtension} from './UrlExtension';
import {TemplateExtension} from './TemplateExtension';
import {EventExtension} from './EventExtension';
import {DataExtension} from './DataExtension';
import {DateExtension} from './DateExtension';
import {CookieExtension} from './CookieExtension';

export class Extension {
    constructor() {
        this.httpRequest  = new HttpRequestExtension();
        this.url          = new UrlExtension();
        this.template     = new TemplateExtension();
        this.event        = new EventExtension();
        this.data         = new DataExtension();
        this.date         = new DateExtension();
        this.cookie       = new CookieExtension();
    }
}
