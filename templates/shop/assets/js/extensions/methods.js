String.prototype.toCebabCase = function () {
    return this.replace(/([a-z0-9]|(?=[A-Z]))([A-Z])/g, '$1-$2').toLowerCase();
};

String.prototype.toCamelCase = function () {
    return this.replace(/(-)+\w/g, function ($1) {
        return $1.replace('-', '').toUpperCase();
    }).replace(/^\w/g, function ($1) {
        return $1.toLowerCase();
    });
};

String.prototype.toPascalCase = function () {
    return this.toCamelCase().replace(/^\w/g, function (w) {
        return w.toUpperCase();
    });
};

String.prototype.capitalize = function () {
    return this.charAt(0).toUpperCase() + this.slice(1);
};

Date.prototype.addHours = function(h) {
    this.setTime(this.getTime() + (h*60*60*1000));
    return this;
};

window.checkObjectsEqual = (a, b) => {
    if (!a && b) {
        return false;
    }

    if (!b && a) {
        return false;
    }

    if (!b && !a) {
        return true;
    }

    let aProps = Object.getOwnPropertyNames(a);
    let bProps = Object.getOwnPropertyNames(b);

    if (aProps.length !== bProps.length) {
        return false;
    }

    for (let i = 0; i < aProps.length; i++) {
        let propName = aProps[i];

        if (a[propName] !== b[propName]) {
            return false;
        }
    }
    return true;
};

Array.prototype.isEqual = function (array) {
    return JSON.stringify(this.sort()) === JSON.stringify(array.sort());
};


window.fb= function (data) {
    if (typeof data === 'object') {
        data = jQuery.extend(true, {}, data);
    }
    console.log(data);
}

window.hashCode = function (hashTarget) {

    let string = hashTarget;

    if (typeof hashTarget === 'object') {
        string = JSON.stringify(string);
    }

    let hash = 0;
    let chr;

    if (string.length === 0) {

        return hash;
    }

    for (let i = 0; i < string.length; i++) {
        chr = string.charCodeAt(i);
        hash = ((hash << 5) - hash) + chr;
        hash |= 0; // Convert to 32bit integer
    }

    return hash;
};

window.sprintf = require('sprintf-js').sprintf;
window.vsprintf = require('sprintf-js').vsprintf;
window.moment = require('moment');
window.moment.locale('ru');

