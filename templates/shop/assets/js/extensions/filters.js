import Vue from 'vue';
import moment from 'moment';
import {Extension} from './Extension';

Vue.filter('formatDate', function (value, format) {
    if (!format) {
        format = 'DD.MM.YYYY, в hh:mm';
    }
    if (value) {
        return moment(String(value)).format(format);
    }
});

Vue.filter('t', function (value, ...args) {
    if (value) {
        return (new Extension()).language.t(String(value), ...args);
    }
});

Vue.filter('plural', function (value, text1, text3, text5) {
    if (value !== undefined && value !== null) {
        return (new Extension()).language.plural(value, text1, text3, text5);
    }
});

Vue.filter('pluralSprintf', function (value, ...args) {
    if (value !== undefined && value !== null) {
        return (new Extension()).language.pluralSprintf(value, ...args);
    }
});

Vue.filter('hashCode', function (value) {
    if (!value) {
        return ''
    }

    return hashCode(value)
});
