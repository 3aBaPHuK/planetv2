export class DateExtension {
    monthsNames() {
        return {
            0: 'Января',
            1: 'Февраля',
            2: 'Марта',
            3: 'Апреля',
            4: 'Мая',
            5: 'Июня',
            6: 'Июля',
            7: 'Августа',
            8: 'Сентября',
            9: 'Октября',
            10: 'Ноября',
            11: 'Декабря'
        };
    }

    dayNames() {
        return {
            0: 'Воскресенье',
            1: 'Понедельник',
            2: 'Вторник',
            3: 'Среда',
            4: 'Четверг',
            5: 'Пятница',
            6: 'Суббота',
        };
    }

    sevenDayNames() {
        return {
            7: 'Воскресенье',
            1: 'Понедельник',
            2: 'Вторник',
            3: 'Среда',
            4: 'Четверг',
            5: 'Пятница',
            6: 'Суббота',
        };
    }

    parseDate(input) {
        const parts = input.split('.');
        return new Date(parts[2], parts[1] - 1, parts[0]);
    }

    formatDate(obj) {
        return `${('0' + obj.getDate()).slice(-2)}.${('0' + (obj.getMonth() + 1)).slice(-2)}.${obj.getFullYear()}`;
    }

    serviceFormattedDate(obj) {
        return `${obj.getFullYear()}-${('0' + (obj.getMonth() + 1)).slice(-2)}-${('0' + obj.getDate()).slice(-2)}`;
    }

    formatTextDate(obj) {
        return `${this.dayNames()[obj.getDay()]}, ${obj.getDate()} ${this.monthsNames()[obj.getMonth()]} ${obj.getFullYear()}`;
    }

    getDayShortCode(number) {
        const numbers = {1: 'mo', 2: 'tu', 3: 'we', 4: 'th', 5: 'fr', 6: 'sa', 0: 'su'};

        return numbers[number];
    }

    getDateNumber(name) {
        const numbers = {mo: 1, tu: 2, we: 3, th: 4, fr: 5, sa: 6, su: 0};

        return numbers[name];
    }

    getWeekNumber(date) {
        date = new Date(Date.UTC(date.getFullYear(), date.getMonth(), date.getDate()));
        date.setUTCDate(date.getUTCDate() + 4 - (date.getUTCDay() || 7));

        let yearStart = new Date(Date.UTC(date.getUTCFullYear(), 0, 1));

        return Math.ceil((((date - yearStart) / 86400000) + 1) / 7);
    }

    getWeekNumbersByMonth(month, year) {
        let days = this.getDaysInMonth(month, year);
        let weeks = [];

        days.forEach((day) => {
            let dayDate = new Date(day);
            let weekNum = this.getWeekNumber(new Date(day));
            if (dayDate.getMonth() === month && weeks.indexOf(weekNum) === -1) {
                weeks.push(weekNum);
            }
        });

        return weeks;
    }

    getDaysInMonth(month, year) {
        let date = new Date(Date.UTC(year, month, 1));
        let days = [];
        while (date.getMonth() === month) {
            days.push(new Date(date));
            date.setDate(date.getDate() + 1);
        }
        return days;
    }

    getDateDay(dateString) {
        return this.getDayShortCode((new Date(dateString)).getDay());
    }
}